module GameName {
    export abstract class Hero extends Entity {
        private keys: Array<Phaser.Key>;
        private heroBodyCenterX: number;
        private heroBodyCenterY: number;

        private buttonTimer: Phaser.Timer;

        private soundsController: SoundController;

        protected healthRegen: number;
        protected manaRegen: number;

        protected fireAllowed: boolean;

        public experience: number;

        public itemsInUse: Array<Item>;

        public level: number;
        public experienceNeededForLevel: number;
        public strength: number;
        public agility: number;
        public intellect: number;
        public startingMana: number;
        public currentMana: number;
        public attackRange: number;
        public gold: number;

        public currentState: HeroStates;
        public heroBody: Phaser.Sprite;
        public interactButtonSprite: Phaser.Sprite;


        public heroClass: string;

        public interactBool: boolean;
        public canHit: boolean;

        public bulletCollection: Array<Arrow>;


        constructor(game, x, y, experience: number, strength: number, agility: number, intellect: number, startingHealth: number,
                    currentHealth: number, startingMana: number, currentMana: number, startingDamage: number,
                    startingArmor: number, startingMoveSpeed: number, keys?, spriteKey?: string, attackRange?: number,
                    idleAnimationFrames?: number[], gestureAnimationFrames?: number[], moveAnimationFrames?: number[],
                    attackAnimationFrames?: number[], deathAnimationFrames?: number[]) {
            super(game, x, y, experience, startingHealth, currentHealth, startingDamage, startingArmor, startingMoveSpeed, 0,
                attackRange, idleAnimationFrames, moveAnimationFrames, attackAnimationFrames);

            this.itemsInUse = [];

            this.initialize(x, y, strength, agility, intellect, startingHealth, currentHealth, startingMana, currentMana,
                startingDamage, startingArmor, startingMoveSpeed, keys, spriteKey, attackRange, idleAnimationFrames,
                gestureAnimationFrames, moveAnimationFrames, attackAnimationFrames, deathAnimationFrames);
        }

        initialize(positionX: number, positionY: number, strength: number, agility: number, intellect: number,
                   startingHealth: number, currentHealth: number, startingMana: number, currentMana: number,
                   startingDamage: number, startingArmor: number, startingMoveSpeed: number, keys: Array<number>,
                   spriteKey: string, attackRange: number, idleAnimationFrames: number[], gestureAnimationFrames: number[],
                   moveAnimationFrames: number[], attackAnimationFrames: number[], deathAnimationFrames: number[]) {

            this.heroClass = spriteKey;
            this.currentState = HeroStates.IDLE;

            this.level = 1;

            this.strength = strength;
            this.agility = agility;
            this.intellect = intellect;
            this.startingHealth = startingHealth;
            this.currentHealth = startingHealth;
            this.startingMana = startingMana;
            this.currentMana = startingMana;
            this.startingDamage = startingDamage;
            this.startingArmor = startingArmor;
            this.startingMoveSpeed = startingMoveSpeed;
            this.attackRange = attackRange;

            this.startingDamage += this.strength * 0.1;
            this.startingMoveSpeed += this.agility * 0.5;

            this.healthRegen = this.strength * 0.01;
            this.manaRegen = this.intellect * 0.01;

            this.gold = 100;

            this.fireAllowed = true;

            this.bulletCollection = [];

            this.heroBody = this.game.add.sprite(positionX, positionY, spriteKey);
            this.heroBody.animations.add("idle", idleAnimationFrames, 8, true);
            this.heroBody.animations.add("gesture", gestureAnimationFrames, 8, false);
            this.heroBody.animations.add("move", moveAnimationFrames, 15, true);
            this.heroBody.animations.add("attack", attackAnimationFrames);
            this.heroBody.animations.add("death", deathAnimationFrames, 8, false);
            this.heroBody.animations.play("idle");
            this.heroBody.anchor.set(0.5, 0.5);

            this.canHit = true;

            this.game.physics.arcade.enable(this.heroBody);
            this.heroBody.body.setSize(12, 26, 10);
            this.dealDamageSignal = new Phaser.Signal();

            this.keys = [];
            for (let i = 0; i < keys.length; i++) {
                this.keys.push(this.game.input.keyboard.addKey(keys[i]))
            }

            this.keys[0].onDown.add(() => {
                this.changeToMoveState();
            });

            this.keys[1].onDown.add(() => {
                this.changeToMoveState();
            });

            this.keys[2].onDown.add(() => {
                this.changeToMoveState();
            });

            this.keys[3].onDown.add(() => {
                this.changeToMoveState();
            });

            this.interactButtonSprite = this.game.add.sprite(null, null, INTERACT_BUTTON_KEY);
            this.interactButtonSprite.alpha = 0;
            this.interactButtonSprite.anchor.set(0.5);
            this.interactButtonSprite.animations.add("interact", [0, 1]);

            this.regenHealthAndMana();

            this.game.time.events.add(10000, () => {
                if (this.currentState === HeroStates.IDLE) {
                    this.heroBody.animations.play("gesture");
                }
            });

            this.soundsController = new SoundController(this.game);
            this.soundsController.create();
        }

        update() {
            this.bulletCollection.forEach((arrow) => {
                arrow.update();
                let distance=Phaser.Math.distance(arrow.x, arrow.y, this.heroBodyCenterX, this.heroBodyCenterY);
                if (distance>=this.attackRange){
                    arrow.destroy();
                    this.bulletCollection.splice(this.bulletCollection.indexOf(arrow),1);
                }
            });

            this.resetVelocity();

            this.experienceNeededForLevel = this.level * 100;
            this.levelUp();

            this.heroBodyCenterX = (this.heroBody.body.x + this.heroBody.body.width / 2);
            this.heroBodyCenterY = (this.heroBody.body.y + this.heroBody.body.height / 2);

            this.heroBody.scale.x = this.followMouse(this.heroBody.position.x);

            this.interactButtonSprite.position.x = this.heroBody.position.x;
            this.interactButtonSprite.position.y = this.heroBody.position.y - this.heroBody.height;

            // this.drawRayCast(this.heroBodyCenterX, this.heroBodyCenterY);
            // For displaying the range of melee attackHero
            // this.drawRange(this.heroBodyCenterX, this.heroBodyCenterY, MELEE_RADIUS);
            this.attack();

            if (this.keys[0].isDown && this.currentState === HeroStates.MOVE) {
                this.heroBody.body.velocity.y -= this.startingMoveSpeed;
            }
            if (this.keys[1].isDown && this.currentState === HeroStates.MOVE) {
                this.heroBody.body.velocity.x -= this.startingMoveSpeed;
            }
            if (this.keys[2].isDown && this.currentState === HeroStates.MOVE) {
                this.heroBody.body.velocity.y = this.startingMoveSpeed;
            }
            if (this.keys[3].isDown && this.currentState === HeroStates.MOVE) {
                this.heroBody.body.velocity.x = this.startingMoveSpeed;
            }

            this.resetIdleState();

            // this.onDamageTaken();

            this.interactWithObject();
            if(this.currentHealth > this.startingHealth){
                this.currentHealth = this.startingHealth;
            }
        }

        destroy() {
            this.bulletCollection.forEach((arrow) => {
                arrow.destroy();
                arrow = null;
            });
            this.bulletCollection = null;
            super.destroy();
        }

        drawRayCast(spriteBodyX: number, spriteBodyY: number): void {
            let x = this.game.input.activePointer.worldX;
            let y = this.game.input.activePointer.worldY;

            this.rayCast.context.clearRect(0, 0, this.game.width, this.game.height);
            this.rayCast.context.beginPath();
            this.rayCast.context.moveTo(spriteBodyX, spriteBodyY);
            this.rayCast.context.lineTo(x, y);
            this.rayCast.context.stroke();
        }

        followMouse(spritePositionX: number): number {
            if (this.game.input.activePointer.worldX > spritePositionX) {
                return this.facingRight = 1;
            }
            else {
                return this.facingRight = -1;
            }
        }

        changeToMoveState() {
            if (this.currentState !== HeroStates.MOVE) {
                this.currentState = HeroStates.MOVE;
                this.heroBody.animations.play("move");
            }
        }

        changeToAttackState() {
            if (this.currentState !== HeroStates.ATTACK) {
                this.currentState = HeroStates.ATTACK;
            }
        }

        resetIdleState() {
            if (this.noKeyIsPressed() && this.currentState === HeroStates.MOVE ||
                this.game.input.activePointer.leftButton.isUp && this.currentState === HeroStates.ATTACK) {
                this.currentState = HeroStates.IDLE;
                this.heroBody.animations.play("idle");
                this.canHit = true;
            }
        }

        resetVelocity() {
            this.heroBody.body.velocity.y = 0;
            this.heroBody.body.velocity.x = 0;
        }

        noKeyIsPressed() {
            for (let i = 0; i < this.keys.length; i++) {
                if (this.keys[i].isDown) {
                    return false;
                }
            }
            return true;
        }

        attack(): void {
            if (this.game.input.activePointer.leftButton.isDown && this.currentState !== HeroStates.ATTACK) {
                this.changeToAttackState();
                this.fire();
                this.heroBody.animations.play("attack", 16, false)
                    .onComplete.addOnce(() => {
                    this.dealDamageSignal.dispatch(this.startingDamage);
                });
            }
            else {
                this.resetFire();
            }
        }

        // may get bugged if enemy hits the hero at the same time
        // onDamageTaken() {
        //     let originalTint = this.heroBody.tint;
        //     if (this.heroBody.tint !== originalTint) {
        //         this.heroBody.tint = originalTint;
        //     }
        //     if (this.isDamageTaken) {
        //         this.game.add.tween(this.heroBody).to({tint: 0xff0000}, 1, Phaser.Easing.Default,
        //             true, 0, 0, true);
        //     }
        //     this.isDamageTaken = false;
        // }

        interactWithObject() {
            if (this.interactBool) {
                this.interactButtonSprite.alpha = 1;
                this.buttonTimer = this.game.time.create();
                this.buttonTimer.loop(250, this.interactButtonAnimation, this);
                this.buttonTimer.start();
            } else {
                this.interactBool = false;
                this.interactButtonSprite.alpha = 0;
            }
        }

        interactButtonAnimation() {
            this.interactButtonSprite.animations.play("interact", 2, false);
        }

        levelUp() {
            if (this.experience >= this.experienceNeededForLevel) {
                this.soundsController.levelUp.play();
                this.level++;
                this.experience -= this.experienceNeededForLevel;
                this.startingHealth += 5;
                this.startingMana += 5;
                this.strength += 1;
                this.agility += 1;
                this.intellect += 1;
            }
        }

        public updateSkills(strength: number, agility: number, intellect: number, mana: number, health: number,
                            armor: number = null, damage: number = null) {
            this.strength += strength;
            this.agility += agility;
            this.intellect += intellect;
            this.startingMana += mana;
            this.currentHealth += health;
            this.startingHealth += health;
            this.startingArmor += armor;
            this.startingDamage += damage;
        }

        fire(): void {
            let mouseX = this.game.input.activePointer.worldX;
            let mouseY = this.game.input.activePointer.worldY;
            let angle = Math.atan2(mouseY - this.heroBodyCenterY, mouseX - this.heroBodyCenterX) * 180 / Math.PI;

            let bullet: Bullet = null;

            // this.bulletCollection.forEach((availableArrow)=> {
            //     if (availableArrow.checkAvailable()){
            //         bullet = availableArrow;
            //     }
            // });

            if (bullet !== null) {
                bullet.resetPosition(bullet.position.clone(), angle);
            }
            else {
                if (this.heroClass === RANGER_HERO_KEY) {
                    bullet = new Arrow(this.game, this.heroBodyCenterX, this.heroBodyCenterY, this.width * 0.18, this.height * 0.35, angle - 90, ARROW_KEY);
                }
                else if (this.heroClass === MAGE_HERO_KEY) {
                    bullet = new FireBall(this.game, this.heroBodyCenterX, this.heroBodyCenterY, this.width * 0.18, this.height * 0.35, angle - 90, ARROW_KEY);
                }
                this.bulletCollection.push(bullet);
                this.game.physics.enable(bullet, Phaser.Physics.ARCADE);
            }

            let startingPosition = this.heroBody.position.clone();
            let directionNormalize = new Phaser.Point(mouseX - startingPosition.x, mouseY - startingPosition.y)
                .normalize();
            let velocity = directionNormalize.multiply(BULLET_VELOCITY, BULLET_VELOCITY);
            let deceleratedVelocity = new Phaser.Point(this.body.velocity.x, this.body.velocity.y).normalize()
                .multiply(BULLET_MOMENTUM_VELOCITY, BULLET_MOMENTUM_VELOCITY).add(velocity.x, velocity.y);

            bullet.shoot(startingPosition, deceleratedVelocity, bullet.angle);
            // }
        }

        regenHealthAndMana() {
            let regenTimer = this.game.time.events.loop(1000, () => {
                if (this.currentHealth < this.startingHealth) {
                    this.currentHealth += this.healthRegen;
                }
                if (this.currentMana < this.startingMana) {
                    this.currentMana += this.manaRegen;
                }
            })
        }

        resetFire(): void {
            this.fireAllowed = true;
        }
    }
}