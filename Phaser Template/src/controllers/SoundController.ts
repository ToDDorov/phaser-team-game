module GameName {
    export class SoundController{
        public townMusic: Phaser.Sound;

        public levelOneMusic: Phaser.Sound;

        public npcGearWelcome: Phaser.Sound;
        public npcGearBuyin: Phaser.Sound;
        public npcGearLeaving: Phaser.Sound;
        public npcGearWiseChoice: Phaser.Sound;

        public npcSkillWelcome: Phaser.Sound;

        public click: Phaser.Sound;
        public menuMusic: Phaser.Sound;

        public buttonClickSound: Phaser.Sound;

        public goldPickup: Phaser.Sound;
        public healthPotionPickup: Phaser.Sound;
        public gemPickup: Phaser.Sound;

        public chestOpen: Phaser.Sound;

        public levelUp: Phaser.Sound;

        public trapSound: Phaser.Sound;
        public leverSound: Phaser.Sound;

        public game: Phaser.Game;

        constructor(game){
            this.game = game;
        }

        create(){
            this.buttonClickSound = this.game.add.audio("menuClick");

            this.click = this.game.add.audio("menuClick");

            this.menuMusic = this.game.add.audio("menuMusic");

            this.townMusic = this.game.add.audio("townMusic");

            this.levelOneMusic = this.game.add.audio("levelOneMusic");

            this.npcGearWelcome = this.game.add.audio("npcGearWelcome");
            this.npcGearBuyin = this.game.add.audio("npcGearBuyin");
            this.npcGearBuyin.volume = 0.5;
            this.npcGearLeaving = this.game.add.audio("npcGearLeaving");
            this.npcGearWiseChoice = this.game.add.audio("npcGearWiseChoice");

            this.goldPickup = this.game.add.audio("goldPickup");
            this.healthPotionPickup = this.game.add.audio("healthPotionSound");
            this.gemPickup = this.game.add.audio("gemSound");

            this.chestOpen = this.game.add.audio("chest");

            this.trapSound = this.game.add.audio("trapSound");
            this.leverSound = this.game.add.sound("leverSound");

            this.levelUp = this.game.add.audio("levelup");

            this.npcSkillWelcome = this.game.add.audio("npcSkillWelcome");
        }
    }
}