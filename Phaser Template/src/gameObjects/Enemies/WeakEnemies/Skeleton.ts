module GameName {
    export class Skeleton extends WeakEnemy {

        constructor(game, x, y, enemyKey, experience, startingHealth: number, currentHealth: number, startingDamage: number,
                    startingArmor: number, startingMoveSpeed: number, visionRange: number, attackRange: number,
                    idleAnimationFrames: number[],gestureAnimationFrames: number[], moveAnimationFrames: number[],
                    attackAnimationFrames: number[], deathAnimationFrames: number[]) {
            super(game, x, y, enemyKey, experience, startingHealth, currentHealth, startingDamage, startingArmor, startingMoveSpeed,
                visionRange, attackRange, idleAnimationFrames, gestureAnimationFrames, moveAnimationFrames,
                attackAnimationFrames, deathAnimationFrames);

            this.initialize(SKELETON_ENEMY_KEY, SKELETON_ENEMY_XP,SKELETON_ENEMY_STARTING_HEALTH, SKELETON_ENEMY_STARTING_HEALTH,
                SKELETON_ENEMY_STARTING_DAMAGE, SKELETON_ENEMY_STARTING_ARMOR, SKELETON_ENEMY_STARTING_MOVE_SPEED,
                SKELETON_ENEMY_VISION_RANGE, SKELETON_ENEMY_ATTACK_RANGE, SKELETON_IDLE_ANIMATION_FRAMES,
                SKELETON_MOVE_ANIMATION_FRAMES, SKELETON_ATTACK_ANIMATION_FRAMES, SKELETON_DEATH_ANIMATION_FRAMES,
                SKELETON_GESTURE_ANIMATION_FRAMES);

            this.enemyBody.body.setSize(25, 32, 4);
            this.enemyBody.scale.set(-1, 1);
            this.defaultFaceDirection = 1;
        }
    }
}