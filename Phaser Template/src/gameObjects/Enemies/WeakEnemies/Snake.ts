module GameName {
    export class Snake extends WeakEnemy {

        constructor(game, x, y, enemyKey, experience, startingHealth: number, currentHealth: number, startingDamage: number,
                    startingArmor: number, startingMoveSpeed: number, visionRange: number, attackRange: number,
                    idleAnimationFrames: number[],gestureAnimationFrames: number[], moveAnimationFrames: number[],
                    attackAnimationFrames: number[], deathAnimationFrames: number[]) {
            super(game, x, y, enemyKey, experience, startingHealth, currentHealth, startingDamage, startingArmor, startingMoveSpeed,
                visionRange, attackRange, idleAnimationFrames, gestureAnimationFrames, moveAnimationFrames,
                attackAnimationFrames, deathAnimationFrames);

            this.initialize(SNAKE_ENEMY_KEY, SNAKE_ENEMY_XP, SNAKE_ENEMY_STARTING_HEALTH, SNAKE_ENEMY_STARTING_HEALTH,
                SNAKE_ENEMY_STARTING_DAMAGE, SNAKE_ENEMY_STARTING_ARMOR, SNAKE_ENEMY_STARTING_MOVE_SPEED,
                SNAKE_ENEMY_VISION_RANGE, SNAKE_ENEMY_ATTACK_RANGE, SNAKE_IDLE_ANIMATION_FRAMES, SNAKE_MOVE_ANIMATION_FRAMES,
                SNAKE_ATTACK_ANIMATION_FRAMES, SNAKE_DEATH_ANIMATION_FRAMES, SNAKE_GESTURE_ANIMATION_FRAMES);

            this.enemyBody.body.setSize(17, 16, 5, 18);
        }
    }
}