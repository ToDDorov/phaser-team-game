/// <reference path = "../../lib/phaser.d.ts"/>

module GameName{
    export class Menu extends Phaser.State{
        private backgroundMenu: Phaser.Image;
        private buttonPlay: Phaser.Image;
        private buttonOptions: Phaser.Image;
        private buttonExit: Phaser.Image;

        public isSoundOn: boolean;
        private soundController: SoundController;

        create(){
            this.soundController = new SoundController(this.game);
            this.soundController.create();

            this.isSoundOn = true;

            this.soundController.menuMusic.play();
            this.soundController.menuMusic.loop = true;

            this.CreateBackground();
            this.CreateButtonPlay();
            this.CreateButtonOptions();
            this.CreateButtonExit();
        }
        private CreateBackground(): void {
            this.backgroundMenu = this.game.add.sprite(0, 0, "backgroundMenu");
        }

        private CreateButtonPlay(): void{
            this.buttonPlay = this.game.add.sprite(this.game.width * 0.1,this.game.height * 0.1,
                "buttonPlay");
            this.buttonPlay.inputEnabled = true;

            this.buttonPlay.events.onInputOver.add(() => {
                this.buttonPlay.tint = TINT_COLOR;
            });

            this.buttonPlay.events.onInputOut.add(() => {
                this.buttonPlay.tint = 0xFFFFFF;
            });

            this.buttonPlay.events.onInputDown.add(() => {
                this.clickIconSound();
                this.game.state.start("SelectHero", true, false, this.soundController.menuMusic);
            });
        }

        private CreateButtonOptions(): void{
            this.buttonOptions = this.game.add.sprite(this.game.width * 0.1,this.game.height * 0.2,
                "buttonOptions");
            this.buttonOptions.inputEnabled = true;

            this.buttonOptions.events.onInputOver.add(() => {
                this.buttonOptions.tint = TINT_COLOR;
            });

            this.buttonOptions.events.onInputOut.add(() => {
                this.buttonOptions.tint = 0xFFFFFF;
            });

            this.buttonOptions.events.onInputDown.add(() => {
                this.clickIconSound();
                if (this.isSoundOn) {
                    this.soundController.menuMusic.volume = 0;
                    this.isSoundOn = false;
                } else {
                    this.soundController.menuMusic.volume = 1;
                    this.isSoundOn = true;
                }
            });
        }

        private CreateButtonExit(): void {
            this.buttonExit = this.game.add.sprite(this.game.width * 0.1, this.game.height * 0.3, "buttonExit");
            this.buttonExit.inputEnabled = true;

            this.buttonExit.events.onInputOver.add(() => {
                this.buttonExit.tint = TINT_COLOR;
            });

            this.buttonExit.events.onInputOut.add(() => {
                this.buttonExit.tint = 0xFFFFFF;
            });

            this.buttonExit.events.onInputDown.add(() => {
                this.game.destroy();
            });
        }

        private clickIconSound(): void {
            if (this.isSoundOn) {
                this.soundController.click.play();
            }
            //} else {
            //    this.game.sound.stopAll();
            //    console.log("stop clicking sound");
            //}
        }





    }
}