module GameName {
    export class YellowSlime extends WeakEnemy {

        constructor(game, x, y, enemyKey, experience, startingHealth: number, currentHealth: number, startingDamage: number,
                    startingArmor: number, startingMoveSpeed: number, visionRange: number, attackRange: number,
                    idleAnimationFrames: number[],gestureAnimationFrames: number[], moveAnimationFrames: number[],
                    attackAnimationFrames: number[], deathAnimationFrames: number[]) {
            super(game, x, y, enemyKey, experience, startingHealth, currentHealth, startingDamage, startingArmor, startingMoveSpeed,
                visionRange, attackRange, idleAnimationFrames, gestureAnimationFrames, moveAnimationFrames,
                attackAnimationFrames, deathAnimationFrames);

            this.initialize(SLIMES_ENEMY_KEY, YELLOW_SLIME_XP, YELLOW_SLIME_ENEMY_STARTING_HEALTH, YELLOW_SLIME_ENEMY_STARTING_HEALTH,
                YELLOW_SLIME_ENEMY_STARTING_DAMAGE, YELLOW_SLIME_ENEMY_STARTING_ARMOR,
                YELLOW_SLIME_ENEMY_STARTING_MOVE_SPEED, YELLOW_SLIME_ENEMY_VISION_RANGE,
                YELLOW_SLIME_ENEMY_ATTACK_RANGE, YELLOW_SLIME_IDLE_ANIMATION_FRAMES, YELLOW_SLIME_MOVE_ANIMATION_FRAMES,
                YELLOW_SLIME_ATTACK_ANIMATION_FRAMES, YELLOW_SLIME_DEATH_ANIMATION_FRAMES,
                YELLOW_SLIME_GESTURE_ANIMATION_FRAMES);

            this.enemyBody.body.setSize(15, 19, 8, 16);
        }
    }
}