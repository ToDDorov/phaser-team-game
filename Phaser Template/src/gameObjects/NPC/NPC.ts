module GameName{
    export class NPC extends Entity{
        sprite: Phaser.Sprite;
        text: Phaser.Text;

         constructor(game, x, y){
            super(game, x, y)
             this.game.physics.enable(this, Phaser.Physics.ARCADE);
         }
    }
}