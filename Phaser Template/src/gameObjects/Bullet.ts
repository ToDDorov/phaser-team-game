module GameName{
    export abstract class Bullet extends Phaser.Sprite{
        private available:boolean;
        public velocity:Phaser.Point;

        constructor(game:Phaser.Game, x:number, y:number, width:number, height:number, angle:number, key:string){
            super(game, x, y, key);
            this.width=width;
            this.height=height;
            this.angle=angle;
            this.anchor.set(0.5);
            this.game.add.existing(this);
            this.available=true;
            this.game.physics.enable(this, Phaser.Physics.ARCADE);
        }

        update(){
            if (this.x > this.game.world.bounds.right || this.x < this.game.world.bounds.left ||
                this.y < this.game.world.bounds.top || this.y > this.game.world.bounds.bottom){
                this.available=true;
            }

            this.position.x += this.velocity.x;
            this.position.y += this.velocity.y;
        }

        destroy(): void {
            this.available = null;
            this.velocity = null;
            super.destroy();
        }

        shoot(position:Phaser.Point, velocity:Phaser.Point, angle:number): void {
            this.available=false;
            this.angle=angle;
            this.position=position;
            this.velocity=velocity;
        }

        checkAvailable(): boolean {
            return this.available;
        }

        resetPosition(newPosition: Phaser.Point, angle: number) : void {
            this.x= newPosition.x;
            this.y= newPosition.y;
            this.angle= angle;
            this.velocity.x=0;
            this.velocity.y=0;
        }

    }
}