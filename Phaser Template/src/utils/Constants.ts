module GameName {
    export const TILE_SIZE: number = 16;

    export enum HeroStates {IDLE, MOVE, ATTACK}

    export enum EnemyStates {IDLE, GESTURE, MOVE, ATTACK, DEATH}

    export const ITEM_SIZE: number = 16;

    export const ITEMS_SHEET_KEY: string = "items";
    export const ITEMS_INFO_KEY: string = "itemsInfo";

    export const HERO_INFO_KEY: string = "heroInfo";

    export const NPC_ATLAS_KEY: string = "npcAtlas";
    export const NPC_GEAR_TEXT_SPEECH: string = "What do u want m8.";
    export const NPC_SKILL_TEXT_SPEECH: string = "Those old bones of mine..";

    export const TOWN_TILEMAP_KEY: string = "town";
    export const TOWN_TILES_KEY: string = "tiles";
    export const TOWN_TILESET_KEY: string = "Town";
    export const LEVEL_ONE_TILEMAP_KEY: string = "newLevelOne";
    export const LEVEL_ONE_TILES_KEY: string = "levelOneTiles";
    export const LEVEL_ONE_TILESET_KEY: string = "tiles";

    export const INTERACT_BUTTON_KEY: string = "interactButton";

    export const MALE_IDLE_ANIMATION_FRAMES: number[] = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9];
    export const MALE_GESTURE_ANIMATION_FRAMES: number[] = [10, 11, 12, 13, 14, 15, 16, 17, 18, 19];
    export const MALE_MOVE_ANIMATION_FRAMES: number[] = [20, 21, 22, 23, 24, 25, 26, 27, 28, 29];
    export const MALE_ATTACK_ANIMATION_FRAMES: number[] = [30, 31, 32, 33, 34, 35, 36, 37, 38, 39];
    export const MALE_DEATH_ANIMATION_FRAMES: number[] = [40, 41, 42, 43, 44, 45, 46, 47, 48, 49];

    export const FEMALE_IDLE_ANIMATION_FRAMES: number[] = [50, 51, 52, 53, 54, 55, 56, 57, 58, 59];
    export const FEMALE_GESTURE_ANIMATION_FRAMES: number[] = [60, 61, 62, 63, 64, 65, 66, 67, 68, 69];
    export const FEMALE_MOVE_ANIMATION_FRAMES: number[] = [70, 71, 72, 73, 74, 75, 76, 77, 78, 79];
    export const FEMALE_ATTACK_ANIMATION_FRAMES: number[] = [80, 87, 82, 83, 84, 85, 86, 87, 88, 89];
    export const FEMALE_DEATH_ANIMATION_FRAMES: number[] = [90, 97, 92, 93, 94, 95, 96, 97, 98, 99];

    export const WARRIOR_HERO_KEY: string = "Warrior";
    export const RANGER_HERO_KEY: string = "Ranger";
    export const ROGUE_HERO_KEY: string = "Rogue";
    export const CLERIC_HERO_KEY: string = "Cleric";
    export const MAGE_HERO_KEY: string = "Mage";

    export const ARROW_KEY: string = "Arrow";
    export const FIREBALL_KEY: string = "Fireball";
    export const BULLET_VELOCITY = 2;
    export const BULLET_MOMENTUM_VELOCITY = -10;

    export const RAT_AND_BAT_ENEMY_KEY: string = "RatAndBat";
    export const RAT_ENEMY_GID: number = 872;
    export const BAT_ENEMY_GID: number = 873;
    export const RAT_IDLE_ANIMATION_FRAMES: number[] = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9];
    export const RAT_GESTURE_ANIMATION_FRAMES: number[] = [10, 11, 12, 13, 14, 15, 16, 17, 18, 19];
    export const RAT_MOVE_ANIMATION_FRAMES: number[] = [20, 21, 22, 23, 24, 25, 26, 27, 28, 29];
    export const RAT_ATTACK_ANIMATION_FRAMES: number[] = [30, 31, 32, 33, 34, 35, 36, 37, 38, 39];
    export const RAT_DEATH_ANIMATION_FRAMES: number[] = [40, 41, 42, 43, 44, 45, 46, 47, 48, 49];
    export const BAT_IDLE_ANIMATION_FRAMES: number[] = [50, 51, 52, 53, 54, 55, 56, 57, 58, 59];
    export const BAT_GESTURE_ANIMATION_FRAMES: number[] = [60, 61, 62, 63, 64, 65, 66, 67, 68, 69];
    export const BAT_MOVE_ANIMATION_FRAMES: number[] = [70, 71, 72, 73, 74, 75, 76, 77, 78, 79];
    export const BAT_ATTACK_ANIMATION_FRAMES: number[] = [80, 87, 82, 83, 84, 85, 86, 87, 88, 89];
    export const BAT_DEATH_ANIMATION_FRAMES: number[] = [90, 97, 92, 93, 94, 95, 96, 97, 98, 99];

    export const SNAKE_ENEMY_KEY: string = "Snake";
    export const SNAKE_ENEMY_GID: number = 871;
    export const SNAKE_IDLE_ANIMATION_FRAMES: number[] = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9];
    export const SNAKE_GESTURE_ANIMATION_FRAMES: number[] = [10, 11, 12, 13, 14, 15, 16, 17, 18, 19];
    export const SNAKE_MOVE_ANIMATION_FRAMES: number[] = [20, 21, 22, 23, 24, 25, 26, 27, 28, 29];
    export const SNAKE_ATTACK_ANIMATION_FRAMES: number[] = [30, 31, 32, 33, 34, 35, 36, 37, 38, 39];
    export const SNAKE_DEATH_ANIMATION_FRAMES: number[] = [40, 41, 42, 43, 44, 45, 46, 47, 48, 49];


    export const SLIMES_ENEMY_KEY: string = "Slimes";
    export const GREEN_SLIME_ENEMY_GID: number = 874;
    export const BLUE_SLIME_ENEMY_GID: number = 875;
    export const RED_SLIME_ENEMY_GID: number = 876;
    export const YELLOW_SLIME_ENEMY_GID: number = 877;
    export const GREEN_SLIME_IDLE_ANIMATION_FRAMES: number[] = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9];
    export const GREEN_SLIME_GESTURE_ANIMATION_FRAMES: number[] = [10, 11, 12, 13, 14, 15, 16, 17, 18, 19];
    export const GREEN_SLIME_MOVE_ANIMATION_FRAMES: number[] = [20, 21, 22, 23, 24, 25, 26, 27, 28, 29];
    export const GREEN_SLIME_ATTACK_ANIMATION_FRAMES: number[] = [30, 31, 32, 33, 34, 35, 36, 37, 38, 39];
    export const GREEN_SLIME_DEATH_ANIMATION_FRAMES: number[] = [50, 51, 52, 53, 54, 55, 56, 57, 58, 59];
    export const BLUE_SLIME_IDLE_ANIMATION_FRAMES: number[] = [50, 51, 52, 53, 54, 55, 56, 57, 58, 59];
    export const BLUE_SLIME_GESTURE_ANIMATION_FRAMES: number[] = [60, 61, 62, 63, 64, 65, 66, 67, 68, 69];
    export const BLUE_SLIME_MOVE_ANIMATION_FRAMES: number[] = [70, 71, 72, 73, 74, 75, 76, 77, 78, 79];
    export const BLUE_SLIME_ATTACK_ANIMATION_FRAMES: number[] = [80, 87, 82, 83, 84, 85, 86, 87, 88, 89];
    export const BLUE_SLIME_DEATH_ANIMATION_FRAMES: number[] = [90, 91, 92, 93, 94, 95, 96, 97, 98, 99];
    export const RED_SLIME_IDLE_ANIMATION_FRAMES: number[] = [100, 101, 102, 103, 104, 105, 106, 107, 108, 109];
    export const RED_SLIME_GESTURE_ANIMATION_FRAMES: number[] = [110, 111, 112, 113, 114, 115, 116, 117, 118, 119];
    export const RED_SLIME_MOVE_ANIMATION_FRAMES: number[] = [120, 121, 122, 123, 124, 125, 126, 127, 128, 129];
    export const RED_SLIME_ATTACK_ANIMATION_FRAMES: number[] = [130, 131, 132, 133, 134, 135, 136, 137, 138, 139];
    export const RED_SLIME_DEATH_ANIMATION_FRAMES: number[] = [140, 141, 142, 143, 144, 145, 146, 147, 148, 149];
    export const YELLOW_SLIME_IDLE_ANIMATION_FRAMES: number[] = [150, 151, 152, 153, 154, 155, 156, 157, 158, 159];
    export const YELLOW_SLIME_GESTURE_ANIMATION_FRAMES: number[] = [160, 161, 162, 163, 164, 165, 166, 167, 168, 169];
    export const YELLOW_SLIME_MOVE_ANIMATION_FRAMES: number[] = [170, 171, 172, 173, 174, 175, 176, 177, 178, 179];
    export const YELLOW_SLIME_ATTACK_ANIMATION_FRAMES: number[] = [180, 181, 182, 183, 184, 185, 186, 187, 188, 189];
    export const YELLOW_SLIME_DEATH_ANIMATION_FRAMES: number[] = [190, 191, 192, 193, 194, 195, 196, 197, 198, 199];

    export const SKELETON_ENEMY_KEY: string = "Skeleton";
    export const SKELETON_ENEMY_GID: number = 883;
    export const SKELETON_IDLE_ANIMATION_FRAMES: number[] = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9];
    export const SKELETON_GESTURE_ANIMATION_FRAMES: number[] = [10, 11, 12, 13, 14, 15, 16, 17, 18, 19];
    export const SKELETON_MOVE_ANIMATION_FRAMES: number[] = [20, 21, 22, 23, 24, 25, 26, 27, 28, 29];
    export const SKELETON_ATTACK_ANIMATION_FRAMES: number[] = [30, 31, 32, 33, 34, 35, 36, 37, 38, 39];
    export const SKELETON_DEATH_ANIMATION_FRAMES: number[] = [40, 41, 42, 43, 44, 45, 46, 47, 48, 49];

    export const GOBLINS_ENEMY_KEY: string = "Goblin";
    export const NAKED_GOBLIN_ENEMY_GID: number = 878;
    export const ARMORED_GOBLIN_ENEMY_GID: number = 879;
    export const NAKED_GOBLIN_IDLE_ANIMATION_FRAMES: number[] = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9];
    export const NAKED_GOBLIN_GESTURE_ANIMATION_FRAMES: number[] = [10, 11, 12, 13, 14, 15, 16, 17, 18, 19];
    export const NAKED_GOBLIN_MOVE_ANIMATION_FRAMES: number[] = [20, 21, 22, 23, 24, 25, 26, 27, 28, 29];
    export const NAKED_GOBLIN_ATTACK_ANIMATION_FRAMES: number[] = [30, 31, 32, 33, 34, 35, 36, 37, 38, 39];
    export const NAKED_GOBLIN_DEATH_ANIMATION_FRAMES: number[] = [40, 41, 42, 43, 44, 45, 46, 47, 48, 49];
    export const ARMORED_GOBLIN_IDLE_ANIMATION_FRAMES: number[] = [50, 51, 52, 53, 54, 55, 56, 57, 58, 59];
    export const ARMORED_GOBLIN_GESTURE_ANIMATION_FRAMES: number[] = [60, 61, 62, 63, 64, 65, 66, 67, 68, 69];
    export const ARMORED_GOBLIN_MOVE_ANIMATION_FRAMES: number[] = [70, 71, 72, 73, 74, 75, 76, 77, 78, 79];
    export const ARMORED_GOBLIN_ATTACK_ANIMATION_FRAMES: number[] = [80, 87, 82, 83, 84, 85, 86, 87, 88, 89];
    export const ARMORED_GOBLIN_DEATH_ANIMATION_FRAMES: number[] = [90, 91, 92, 93, 94, 95, 96, 97, 98, 99];

    export const MINOTAUR_ENEMY_KEY: string = "Minotaur";
    export const MINOTAUR_ENEMY_GID: number = 920;
    export const MINOTAUR_IDLE_ANIMATION_FRAMES: number[] = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9];
    export const MINOTAUR_GESTURE_ANIMATION_FRAMES: number[] = [10, 11, 12, 13, 14, 15, 16, 17, 18, 19];
    export const MINOTAUR_MOVE_ANIMATION_FRAMES: number[] = [20, 21, 22, 23, 24, 25, 26, 27, 28, 29];
    export const MINOTAUR_ATTACK_ANIMATION_FRAMES: number[] = [30, 31, 32, 33, 34, 35, 36, 37, 38, 39];
    export const MINOTAUR_DEATH_ANIMATION_FRAMES: number[] = [40, 41, 42, 43, 44, 45, 46, 47, 48, 49];

    export const ORCS_ENEMY_KEY: string = "Orcs";
    export const CAPED_ORC_ENEMY_GID: number = 885;
    export const MAGE_ORC_ENEMY_GID: number = 886;
    export const CAPED_ORC_IDLE_ANIMATION_FRAMES: number[] = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9];
    export const CAPED_ORC_GESTURE_ANIMATION_FRAMES: number[] = [10, 11, 12, 13, 14, 15, 16, 17, 18, 19];
    export const CAPED_ORC_MOVE_ANIMATION_FRAMES: number[] = [20, 21, 22, 23, 24, 25, 26, 27, 28, 29];
    export const CAPED_ORC_ATTACK_ANIMATION_FRAMES: number[] = [30, 31, 32, 33, 34, 35, 36, 37, 38, 39];
    export const CAPED_ORC_DEATH_ANIMATION_FRAMES: number[] = [40, 41, 42, 43, 44, 45, 46, 47, 48, 49];
    export const MAGE_ORC_IDLE_ANIMATION_FRAMES: number[] = [50, 51, 52, 53, 54, 55, 56, 57, 58, 59];
    export const MAGE_ORC_GESTURE_ANIMATION_FRAMES: number[] = [60, 61, 62, 63, 64, 65, 66, 67, 68, 69];
    export const MAGE_ORC_MOVE_ANIMATION_FRAMES: number[] = [70, 71, 72, 73, 74, 75, 76, 77, 78, 79];
    export const MAGE_ORC_ATTACK_ANIMATION_FRAMES: number[] = [80, 87, 82, 83, 84, 85, 86, 87, 88, 89];
    export const MAGE_ORC_DEATH_ANIMATION_FRAMES: number[] = [90, 91, 92, 93, 94, 95, 96, 97, 98, 99];


    export const STATUE_ENEMY_KEY: string = "Statue";
    export const STATUE_ENEMY_GID: number = 627;
    export const STATUE_IDLE_ANIMATION_FRAMES: number[] = [0, 1, 2, 3];
    export const STATUE_MOVE_ANIMATION_FRAMES: number[] = [8, 9, 10, 11, 12, 13, 14, 15];
    export const STATUE_ATTACK_ANIMATION_FRAMES: number[] = [16, 17, 18, 19, 20];
    export const STATUE_DEATH_ANIMATION_FRAMES: number[] = [24, 25, 26, 27, 28, 29];

    export const MOSS_STATUE_ENEMY_KEY: string = "StatueMoss";
    export const MOSS_STATUE_ENEMY_GID: number = 628;
    export const MOSS_STATUE_IDLE_ANIMATION_FRAMES: number[] = [0, 1, 2, 3];
    export const MOSS_STATUE_MOVE_ANIMATION_FRAMES: number[] = [8, 9, 10, 11, 12, 13, 14, 15];
    export const MOSS_STATUE_ATTACK_ANIMATION_FRAMES: number[] = [16, 17, 18, 19, 20];
    export const MOSS_STATUE_DEATH_ANIMATION_FRAMES: number[] = [24, 25, 26, 27, 28, 29];

    export const MELEE_RADIUS: number = 40;
    export const RANGE_RADIUS: number = 150;
    export const STARTING_EXPERIENCE = 0;
    export const PLAYER_CONTROL = [Phaser.Keyboard.W, Phaser.Keyboard.A, Phaser.Keyboard.S, Phaser.Keyboard.D];

    export const RAT_ENEMY_STARTING_HEALTH: number = 30;
    export const RAT_ENEMY_STARTING_DAMAGE: number = 10;
    export const RAT_ENEMY_STARTING_ARMOR: number = 5;
    export const RAT_ENEMY_STARTING_MOVE_SPEED: number = 100;
    export const RAT_ENEMY_ATTACK_RANGE: number = 30;
    export const RAT_ENEMY_VISION_RANGE: number = 250;
    export const RAT_ENEMY_XP: number = 10;

    export const BAT_ENEMY_STARTING_HEALTH: number = 30;
    export const BAT_ENEMY_STARTING_DAMAGE: number = 10;
    export const BAT_ENEMY_STARTING_ARMOR: number = 5;
    export const BAT_ENEMY_STARTING_MOVE_SPEED: number = 100;
    export const BAT_ENEMY_ATTACK_RANGE: number = 30;
    export const BAT_ENEMY_VISION_RANGE: number = 500;
    export const BAT_ENEMY_XP: number = 10;

    export const SNAKE_ENEMY_STARTING_HEALTH: number = 30;
    export const SNAKE_ENEMY_STARTING_DAMAGE: number = 10;
    export const SNAKE_ENEMY_STARTING_POISON_DAMAGE: number = 1;
    export const SNAKE_ENEMY_STARTING_ARMOR: number = 5;
    export const SNAKE_ENEMY_STARTING_MOVE_SPEED: number = 100;
    export const SNAKE_ENEMY_ATTACK_RANGE: number = 35;
    export const SNAKE_ENEMY_VISION_RANGE: number = 200;
    export const SNAKE_ENEMY_XP: number = 10;

    export const GREEN_SLIME_ENEMY_STARTING_HEALTH: number = 20;
    export const GREEN_SLIME_ENEMY_STARTING_DAMAGE: number = 15;
    export const GREEN_SLIME_ENEMY_STARTING_ARMOR: number = 5;
    export const GREEN_SLIME_ENEMY_STARTING_MOVE_SPEED: number = 75;
    export const GREEN_SLIME_ENEMY_ATTACK_RANGE: number = 50;
    export const GREEN_SLIME_ENEMY_VISION_RANGE: number = 350;
    export const GREEN_SLIME_ENEMY_XP: number = 10;

    export const BLUE_SLIME_ENEMY_STARTING_HEALTH: number = 20;
    export const BLUE_SLIME_ENEMY_STARTING_DAMAGE: number = 15;
    export const BLUE_SLIME_ENEMY_STARTING_ARMOR: number = 5;
    export const BLUE_SLIME_ENEMY_STARTING_MOVE_SPEED: number = 100;
    export const BLUE_SLIME_ENEMY_ATTACK_RANGE: number = 50;
    export const BLUE_SLIME_ENEMY_VISION_RANGE: number = 350;
    export const BLUE_SLIME_XP: number = 20;

    export const RED_SLIME_ENEMY_STARTING_HEALTH: number = 20;
    export const RED_SLIME_ENEMY_STARTING_DAMAGE: number = 15;
    export const RED_SLIME_ENEMY_STARTING_ARMOR: number = 5;
    export const RED_SLIME_ENEMY_STARTING_MOVE_SPEED: number = 100;
    export const RED_SLIME_ENEMY_ATTACK_RANGE: number = 50;
    export const RED_SLIME_ENEMY_VISION_RANGE: number = 350;
    export const RED_SLIME_XP: number = 20;

    export const YELLOW_SLIME_ENEMY_STARTING_HEALTH: number = 20;
    export const YELLOW_SLIME_ENEMY_STARTING_DAMAGE: number = 17;
    export const YELLOW_SLIME_ENEMY_STARTING_ARMOR: number = 5;
    export const YELLOW_SLIME_ENEMY_STARTING_MOVE_SPEED: number = 100;
    export const YELLOW_SLIME_ENEMY_ATTACK_RANGE: number = 50;
    export const YELLOW_SLIME_ENEMY_VISION_RANGE: number = 400;
    export const YELLOW_SLIME_XP: number = 20;

    export const NAKED_GOBLIN_ENEMY_STARTING_HEALTH: number = 60;
    export const NAKED_GOBLIN_ENEMY_STARTING_DAMAGE: number = 18;
    export const NAKED_GOBLIN_ENEMY_STARTING_ARMOR: number = 7;
    export const NAKED_GOBLIN_ENEMY_STARTING_MOVE_SPEED: number = 80;
    export const NAKED_GOBLIN_ENEMY_ATTACK_RANGE: number = 20;
    export const NAKED_GOBLIN_ENEMY_VISION_RANGE: number = 200;
    export const NAKED_GOBLIN_ENEMY_XP: number = 22;

    export const ARMORED_GOBLIN_ENEMY_STARTING_HEALTH: number = 65;
    export const ARMORED_GOBLIN_ENEMY_STARTING_DAMAGE: number = 20;
    export const ARMORED_GOBLIN_ENEMY_STARTING_ARMOR: number = 10;
    export const ARMORED_GOBLIN_ENEMY_STARTING_MOVE_SPEED: number = 100;
    export const ARMORED_GOBLIN_ENEMY_ATTACK_RANGE: number = 30;
    export const ARMORED_GOBLIN_ENEMY_VISION_RANGE: number = 200;
    export const ARMORED_GOBLIN_ENEMY_XP: number = 20;

    export const SKELETON_ENEMY_STARTING_HEALTH: number = 40;
    export const SKELETON_ENEMY_STARTING_DAMAGE: number = 25;
    export const SKELETON_ENEMY_STARTING_ARMOR: number = 5;
    export const SKELETON_ENEMY_STARTING_MOVE_SPEED: number = 100;
    export const SKELETON_ENEMY_ATTACK_RANGE: number = 30;
    export const SKELETON_ENEMY_VISION_RANGE: number = 300;
    export const SKELETON_ENEMY_XP: number = 30;

    export const CAPED_ORC_ENEMY_STARTING_HEALTH: number = 20;
    export const CAPED_ORC_ENEMY_STARTING_DAMAGE: number = 5;
    export const CAPED_ORC_ENEMY_STARTING_ARMOR: number = 5;
    export const CAPED_ORC_ENEMY_STARTING_MOVE_SPEED: number = 100;
    export const CAPED_ORC_ENEMY_ATTACK_RANGE: number = 30;
    export const CAPED_ORC_ENEMY_VISION_RANGE: number = 200;
    export const CAPED_ORC_ENEMY_XP: number = 10;

    export const MAGE_ORC_ENEMY_STARTING_HEALTH: number = 20;
    export const MAGE_ORC_ENEMY_STARTING_DAMAGE: number = 5;
    export const MAGE_ORC_ENEMY_STARTING_ARMOR: number = 5;
    export const MAGE_ORC_ENEMY_STARTING_MOVE_SPEED: number = 100;
    export const MAGE_ORC_ENEMY_ATTACK_RANGE: number = 30;
    export const MAGE_ORC_ENEMY_VISION_RANGE: number = 200;
    export const MAGE_ORC_ENEMY_XP: number = 10;

    export const MINOTAUR_ENEMY_STARTING_HEALTH: number = 200;
    export const MINOTAUR_ENEMY_STARTING_DAMAGE: number = 15;
    export const MINOTAUR_ENEMY_STARTING_ARMOR: number = 5;
    export const MINOTAUR_ENEMY_STARTING_MOVE_SPEED: number = 150;
    export const MINOTAUR_ENEMY_ATTACK_RANGE: number = 30;
    export const MINOTAUR_ENEMY_VISION_RANGE: number = 200;
    export const MINOTAUR_ENEMY_XP: number = 100;

    export const STATUE_ENEMY_STARTING_HEALTH: number = 20;
    export const STATUE_ENEMY_STARTING_DAMAGE: number = 5;
    export const STATUE_ENEMY_STARTING_ARMOR: number = 5;
    export const STATUE_ENEMY_STARTING_MOVE_SPEED: number = 100;
    export const STATUE_ENEMY_ATTACK_RANGE: number = 30;
    export const STATUE_ENEMY_VISION_RANGE: number = 200;
    export const STATUE_ENEMY_XP: number = 200;

    export const MOSS_STATUE_ENEMY_STARTING_HEALTH: number = 20;
    export const MOSS_STATUE_ENEMY_STARTING_DAMAGE: number = 5;
    export const MOSS_STATUE_ENEMY_STARTING_ARMOR: number = 5;
    export const MOSS_STATUE_ENEMY_STARTING_MOVE_SPEED: number = 100;
    export const MOSS_STATUE_ENEMY_ATTACK_RANGE: number = 30;
    export const MOSS_STATUE_ENEMY_VISION_RANGE: number = 200;
    export const MOSS_STATUE_ENEMY_XP: number = 10;

    export const NUM_OF_SKILL_ITEMS: number = 9;
    export const NUM_OF_GEAR_ITEMS: number = 24;

    export const TINT_COLOR: number = 0xffcc66;

    //SelectHero
    export const BORDER_WIDTH = 2;
    export const GLOW_WIDTH = 1;
}