module GameName{
     export class Arrow extends Bullet{
         constructor(game:Phaser.Game, x:number, y:number, width:number, height:number, angle:number, key:string){
             super(game, x, y, width, height, angle, ARROW_KEY);
         }
     }
}