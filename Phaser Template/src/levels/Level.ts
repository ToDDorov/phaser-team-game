/// <reference path = "../../lib/phaser.d.ts"/>
Phaser.Tilemap.prototype.setCollisionBetween =  function (start, stop, collides, layer, recalculate) {

    if (collides === undefined) {
        collides = true;
    }
    if (layer === undefined) {
        layer = this.currentLayer;
    }
    if (recalculate === undefined) {
        recalculate = true;
    }

    layer = this.getLayer(layer);

    for (let index = start; index <= stop; index++) {
        if (collides) {
            this.collideIndexes.push(index);
        }
        else {
            let i = this.collideIndexes.indexOf(index);

            if (i > -1) {
                this.collideIndexes.splice(i, 1);
            }
        }
    }

    for (let y = 0; y < this.layers[layer].height; y++) {
        for (let x = 0; x < this.layers[layer].width; x++) {
            let tile = this.layers[layer].data[y][x];

            if (tile && tile.index >= start && tile.index <= stop) {
                if (collides) {
                    tile.setCollision(true, true, true, true);
                }
                else {
                    tile.resetCollision();
                }

                tile.faceTop = collides;
                tile.faceBottom = collides;
                tile.faceLeft = collides;
                tile.faceRight = collides;
            }
        }
    }

    if (recalculate) {
        //  Now re-calculate interesting faces
        this.calculateFaces(layer);
    }

    return layer;

};

module GameName {
    export class Level {
        game: Phaser.Game;
        map: Phaser.Tilemap;
        floor: Phaser.TilemapLayer;
        collisionBlocks: Phaser.TilemapLayer;
        walls: Phaser.TilemapLayer;
        roof: Phaser.TilemapLayer;
        decor: Phaser.TilemapLayer;
        group: Phaser.Group;

        constructor(game: Phaser.Game) {
            this.game = game;
        }

        create() {
            this.group = this.game.add.group();
        }

        getObjectFromTile(objectGroup: string, objectName: string) {
            let myObject;
            this.map.objects[objectGroup].forEach(object => {
                if (object.name === objectName) {
                    myObject = object;
                }
            })
            return myObject;
        }
    }
}