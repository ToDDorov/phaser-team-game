module GameName {
    export class LevelOneObjectFactory {
        game: Phaser.Game;

        private map: Phaser.Tilemap;
        private soundController: SoundController;

        public enemies: Phaser.Group;
        public goldGroup: Phaser.Group;
        public potionGroup: Phaser.Group;
        public chestGroup: Phaser.Group;
        public gemGroup: Phaser.Group;
        public torchGroup: Phaser.Group;
        public fireGroup: Phaser.Group;
        public leverGroup: Phaser.Group;

        public minotaurBox: Phaser.Rectangle;
        public chestBox: Phaser.Rectangle;
        public leverBox: Phaser.Rectangle;
        public exitBox: Phaser.Rectangle;

        public minotaurStatue: Phaser.Sprite;
        public gem: Phaser.Sprite;

        public intersectStatueSignal: Phaser.Signal;
        public goldPickUpSignal: Phaser.Signal;
        public gemPickUpSignal: Phaser.Signal;
        public potionPickUpSignal: Phaser.Signal;

        public exitText: Phaser.Text;

        public hasIntersected: boolean;
        public isExitOpen: boolean;

        public minotaur: Minotaur;

        public level: Level;


        constructor(game, levelOneMap: Phaser.Tilemap, level: Level) {
            this.game = game;
            this.map = levelOneMap;
            this.level = level;

            this.enemies = this.game.add.group();

            this.soundController = new SoundController(this.game);
        }

        create() {
            this.soundController.create();

            this.createEnemies();
            this.createStatueAndMinotaur();
            this.createGold();
            this.createChest();
            this.createGem();
            this.createTorches();
            this.createFire();
            this.createLever();
            this.createPotions();
            this.createExit();
        }

        createEnemies(): void {

            this.map.createFromObjects("entities", RAT_ENEMY_GID, "emptySprite", null, false,
                true, this.enemies, Rat);
            this.map.createFromObjects("entities", BAT_ENEMY_GID, "emptySprite", null, false,
                true, this.enemies, Bat);
            this.map.createFromObjects("entities", SNAKE_ENEMY_GID, "emptySprite", null, false,
                true, this.enemies, Snake);
            this.map.createFromObjects("entities", SKELETON_ENEMY_GID, "emptySprite", null, false,
                true, this.enemies, Skeleton);
            this.map.createFromObjects("entities", MINOTAUR_ENEMY_GID, "emptySprite", null, false,
                false, this.enemies, Minotaur);
            this.map.createFromObjects("entities", NAKED_GOBLIN_ENEMY_GID, "emptySprite", null, false,
                true, this.enemies, NakedGoblin);
            this.map.createFromObjects("entities", ARMORED_GOBLIN_ENEMY_GID, "emptySprite", null, false,
                true, this.enemies, ArmoredGoblin);
            this.map.createFromObjects("entities", CAPED_ORC_ENEMY_GID, "emptySprite", null, false,
                true, this.enemies, CapedOrc);
            this.map.createFromObjects("entities", MAGE_ORC_ENEMY_GID, "emptySprite", null, false,
                true, this.enemies, MageOrc);
            this.map.createFromObjects("entities", GREEN_SLIME_ENEMY_GID, "emptySprite", null, false,
                false, this.enemies, GreenSlime);
            this.map.createFromObjects("entities", BLUE_SLIME_ENEMY_GID, "emptySprite", null, false,
                true, this.enemies, BlueSlime);
            this.map.createFromObjects("entities", RED_SLIME_ENEMY_GID, "emptySprite", null, false,
                true, this.enemies, RedSlime);
            this.map.createFromObjects("entities", YELLOW_SLIME_ENEMY_GID, "emptySprite", null, false,
                true, this.enemies, YellowSlime);
            // this.map.createFromObjects("entities", STATUE_ENEMY_GID, "emptySprite", null, false,
            //     false, this.enemies, Statue);
            // this.map.createFromObjects("entities", MOSS_STATUE_ENEMY_GID, "emptySprite", null, false,
            //     false, this.enemies, MossStatue);
        }

        createStatueAndMinotaur() {
            this.intersectStatueSignal = new Phaser.Signal();

            let minotaurBoxObjectTiled = this.level.getObjectFromTile("decor", "minotaurBox");
            this.minotaurBox = new Phaser.Rectangle(minotaurBoxObjectTiled.x, minotaurBoxObjectTiled.y,
                minotaurBoxObjectTiled.properties.width, minotaurBoxObjectTiled.properties.height);

            let minotaurStatueObjectTiled = this.level.getObjectFromTile("decor", "minotaurStatue");
            this.minotaurStatue = this.game.make.sprite(minotaurStatueObjectTiled.x, minotaurStatueObjectTiled.y,
                "minotaurStatue");
            this.game.physics.arcade.enable(this.minotaurStatue);
            this.minotaurStatue.body.immovable = true;

            let minotaurObjectTiled = this.level.getObjectFromTile("entities", "minotaur");
            this.minotaur = new Minotaur(this.game, minotaurObjectTiled.x, minotaurObjectTiled.y,
                MINOTAUR_ENEMY_KEY, 30, MINOTAUR_ENEMY_STARTING_HEALTH, MINOTAUR_ENEMY_STARTING_HEALTH,
                MINOTAUR_ENEMY_STARTING_DAMAGE, MINOTAUR_ENEMY_STARTING_ARMOR, MINOTAUR_ENEMY_STARTING_MOVE_SPEED,
                MINOTAUR_ENEMY_VISION_RANGE, MINOTAUR_ENEMY_ATTACK_RANGE, MINOTAUR_IDLE_ANIMATION_FRAMES,
                MINOTAUR_MOVE_ANIMATION_FRAMES, MINOTAUR_ATTACK_ANIMATION_FRAMES, MINOTAUR_DEATH_ANIMATION_FRAMES,
                MINOTAUR_GESTURE_ANIMATION_FRAMES);
            this.minotaur.enemyBody.kill();

            this.enemies.add(this.minotaur);
        }

        intersectsCowBox(heroSprite: Phaser.Sprite, key: Phaser.Key, player: Hero): void {
            if (this.minotaurBox !== null) {
                if (this.minotaurBox.intersects(heroSprite.body, 0)) {
                    player.interactBool = true;

                    if (key.isDown) {
                        this.soundController.trapSound.play();
                        this.hasIntersected = true;
                        this.intersectStatueSignal.dispatch(this.hasIntersected);
                    }
                }
                else {
                    player.interactBool = false;
                }
            }
        }

        tweenStatue(hasIntersected: boolean) {
            if (hasIntersected) {
                this.game.add.tween(this.minotaurStatue.position).to({x: this.minotaurStatue.position.x - 1},
                    50, Phaser.Easing.Default, true, null, 10, true)
                    .onComplete.add(() => {
                    this.game.add.tween(this.minotaurStatue.scale).to({y: 0}, 100,
                        Phaser.Easing.Default, true)
                        .onComplete.add(() => {
                        this.minotaurStatue.destroy();
                        this.minotaurBox = null;
                        this.minotaur.enemyBody.revive();
                    }, this);
                }, this)
            }
        }

        createGold(): void {
            this.goldPickUpSignal = new Phaser.Signal();

            this.goldGroup = this.game.add.physicsGroup();
            this.map.createFromObjects("items", 662, "goldBig", null, true, false,
                this.goldGroup);
        }

        createPotions(): void {
            this.potionPickUpSignal = new Phaser.Signal();

            this.potionGroup = this.game.add.physicsGroup();
            this.map.createFromObjects("items", 721, "healthPotion", null, true, true,
                this.potionGroup);
        }

        pickGoldUp(player: Phaser.Sprite): void {
            this.goldGroup.forEach((gold) => {
                this.game.physics.arcade.overlap(player, gold, () => {
                    this.soundController.goldPickup.play();
                    this.goldPickUpSignal.dispatch(gold.value);
                    gold.destroy();
                }, null, this);
            }, this);
        }

        pickPotionUp(player: Phaser.Sprite): void {
            this.potionGroup.forEach((potion) => {
                this.game.physics.arcade.overlap(player, potion, () => {
                    this.soundController.healthPotionPickup.play();
                    this.potionPickUpSignal.dispatch(potion.value);
                    potion.destroy();
                }, null, this);
            }, this)
        }

        createTorches() {
            this.torchGroup = this.game.add.physicsGroup();
            this.map.createFromObjects("decor", 582, "torch", null, true, true,
                this.torchGroup);
            this.map.createFromObjects("decor", 581, "torch", null, true, true,
                this.torchGroup);

            this.torchGroup.forEach((torch) => {
                torch.animations.add("glow", [1, 2, 3], 7, true);
                torch.animations.play("glow");
            }, this);
        }

        createFire() {
            this.fireGroup = this.game.add.physicsGroup();
            this.map.createFromObjects("decor", 975, "fire", null, true, true,
                this.fireGroup);

            this.fireGroup.forEach((fire) => {
                fire.animations.add("burn", [0, 1, 2], 8, true);
                fire.animations.play("burn");
            }, this);
        }

        createGem(): void {
            this.gemPickUpSignal = new Phaser.Signal();
            this.gemGroup = this.game.add.physicsGroup();

            this.gem = this.game.add.sprite(0, 0, "gemGreen");
            this.gem.kill();
            this.gemGroup.add(this.gem);
        }

        pickGemUp(player: Phaser.Sprite): void {
            this.gemGroup.forEach((gem) => {
                this.game.physics.arcade.overlap(player, gem, () => {
                    this.soundController.gemPickup.play();
                    this.gemPickUpSignal.dispatch(gem.value);
                    gem.destroy();
                }, null, this);
            }, this);
        }

        createChest(): void {
            this.chestGroup = this.game.add.physicsGroup();

            this.map.createFromObjects("items", 573, "chest", null, true, false,
                this.chestGroup);

            let chestBoxObjectTiled = this.level.getObjectFromTile("items", "chestBox");
            this.chestBox = new Phaser.Rectangle(chestBoxObjectTiled.x, chestBoxObjectTiled.y,
                chestBoxObjectTiled.properties.width, chestBoxObjectTiled.properties.height);

            this.chestGroup.forEach((chest) => {
                chest.animations.add("open", [1]);
                chest.body.immovable = true;
            }, this);
        }

        createLever() {
            this.leverGroup = this.game.add.physicsGroup();

            this.map.createFromObjects("items", 586, "lever", null, true, true,
                this.leverGroup);

            let leverBoxObejctTiled = this.level.getObjectFromTile("items", "leverBox");
            this.leverBox = new Phaser.Rectangle(leverBoxObejctTiled.x, leverBoxObejctTiled.y,
                leverBoxObejctTiled.properties.width, leverBoxObejctTiled.properties.height);

            this.leverGroup.forEach((lever) => {
                lever.animations.add("switch", [1]);
                lever.body.immovable = true;
            }, this);
        }

        createExit() {
            let exitBoxObjectTiled = this.level.getObjectFromTile("items", "exit");
            this.exitBox = new Phaser.Rectangle(exitBoxObjectTiled.x, exitBoxObjectTiled.y,
                exitBoxObjectTiled.width, exitBoxObjectTiled.height);

            let style = {font: "15px Revalia", fill: "#d57c2d", align: "center", stroke: '#000000', strokeThickness: 2};

            let exitTextObjectTiled = this.level.getObjectFromTile("items", "exitText");
            this.exitText = this.game.add.text(exitTextObjectTiled.x, exitTextObjectTiled.y,
                "You need to find a leverSound!", style);
            this.exitText.kill();
        }

        intersectsExitBox(heroSprite: Phaser.Sprite, key: Phaser.Key, player: Hero): void {
            if (this.exitBox !== null) {
                if (this.exitBox.intersects(heroSprite.body, 0)) {
                    player.interactBool = true;

                    if (key.isDown) {
                        if (this.isExitOpen) {
                            console.log("You won");
                        } else {
                            this.exitText.revive();
                        }
                    }
                }
                else {
                    this.exitText.kill();
                }
            }
        }

        activateLever(heroBody: Phaser.Sprite, key: Phaser.Key, player: Hero) {
            if (this.leverBox !== null) {
                if (this.leverBox.intersects(heroBody.body, 0)) {
                    player.interactBool = true;
                    if (key.isDown) {
                        this.soundController.leverSound.play();
                        this.isExitOpen = true;
                        this.leverGroup.forEach((lever) => {
                            lever.animations.play("switch");
                        }, this);
                    }
                }
                else {
                    player.interactBool = false;
                }
            }
        }

        openChest(heroBody: Phaser.Sprite, key: Phaser.Key, player: Hero) {
            if (this.chestBox !== null) {
                if (this.chestBox.intersects(heroBody.body, 0)) {
                    player.interactBool = true;
                    if (key.isDown) {
                        this.soundController.chestOpen.play();
                        this.chestGroup.forEach((chest) => {
                            chest.animations.play("open");
                            this.gem.position.x = chest.position.x + chest.width;
                            this.gem.position.y = chest.position.y;
                            this.gem.revive();
                            this.game.add.tween(this.gem.position).to({x: this.gem.position.x + 3}, 600,
                                Phaser.Easing.Default, true);
                            this.game.add.tween(this.gem.position).to({y: this.gem.position.y - 1}, 400,
                                Phaser.Easing.Default, true)
                                .onComplete.add(() => {
                                this.game.add.tween(this.gem.position).to({y: this.gem.position.y + 4}, 200,
                                    Phaser.Easing.Default, true);
                            }, this);
                            this.chestBox = null;
                        }, this);
                    }
                }
                else {
                    player.interactBool = false;
                }
            }
        }
    }
}