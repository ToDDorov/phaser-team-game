module GameName {
    export class SelectHero extends Phaser.State {
        private bmd: Phaser.BitmapData;

        private heroSprite: Phaser.Sprite;
        private maleButton: Phaser.Sprite;
        private femaleButton: Phaser.Sprite;
        private nameWindow: Phaser.Sprite;
        private storyWindow: Phaser.Sprite;

        private heroIndex: number;
        private lineWidth: number;

        private allMaleFrames: Array<number>;
        private allFemaleFrames: Array<number>;
        private playerArray: Array<string>;
        private heroArrayInfo: Array<any>;

        private style: {};

        private textGroup: Phaser.Group;
        private nameGroup: Phaser.Group;
        private prosAndConsGroup: Phaser.Group;

        private heroInfo: any;
        private currentHero: any;

        private strokeStyle: string;
        private currentGenderFrames: string;
        private currentSelectedClass: string;
        private currentSelectedGender: string;

        private soundController: SoundController;
        private menuMusic: Phaser.Sound;

        init(menuMusic){
            this.menuMusic = menuMusic;
        }

        create() {
            this.soundController = new SoundController(this.game);
            this.soundController.create();
            this.soundController.menuMusic = this.menuMusic;

            this.style = {font: "16px VT323", fill: "#E9802C", stroke: "#772A17", strokeThickness: 4};
            this.strokeStyle = "#CA630C";
            this.heroIndex = 0;

            this.createBackground();
            this.createHeroArray();
            this.drawHeroWindow();
            this.drawHeroArrows();
            this.drawStatsWindow();
            this.drawNameWindow();
            this.drawMaleButton();
            this.drawFemaleButton();
            this.drawSelectButton();
            this.drawProsAndConsWindow();
            this.drawStoryWindow();

        }

        update() {
            this.updateSelection();
        }

        createBackground(){
            let backgroundImage = this.game.add.image(0,0, "selectScreenBackground");
        }

        drawHeroWindow() {
            this.bmd = this.make.bitmapData(this.game.width * 0.3, this.game.height * 0.3,
                "bmdHeroRect", true);
            this.lineWidth = BORDER_WIDTH;
            this.bmd.ctx.lineWidth = this.lineWidth;
            this.bmd.ctx.strokeStyle = this.strokeStyle;
            this.bmd.ctx.strokeRect(this.lineWidth, this.lineWidth,
                this.bmd.width - 2 * this.lineWidth, this.bmd.height - 2 * this.lineWidth);

            let heroWindow = this.game.add.sprite(this.game.width * 0.25, this.game.height * 0.2
                , this.game.cache.getBitmapData("bmdHeroRect"));
            heroWindow.anchor.set(0.5, 0.5);

            this.currentHero = this.heroArrayInfo[this.heroIndex];
            this.currentSelectedClass = this.currentHero.heroClass;

            this.heroSprite = this.game.add.sprite(heroWindow.x, heroWindow.y, this.currentSelectedClass);
            this.heroSprite.scale.set(2, 2);
            this.heroSprite.anchor.set(0.5, 0.5);
            this.allMaleFrames = [];
            this.allFemaleFrames = [];
            for (let i = 0; i < 50; i++) {
                this.allMaleFrames.push(i);
            }
            for (let i = 50; i < 100; i++) {
                this.allFemaleFrames.push(i);
            }

            this.currentGenderFrames = "allMaleAnimations";
            this.heroSprite.animations.add("allMaleAnimations", this.allMaleFrames, 8, true);
            this.heroSprite.animations.add("allFemaleAnimations", this.allFemaleFrames, 8, true);
            this.heroSprite.animations.play("allMaleAnimations");
        }

        drawHeroArrows() {
            let border = BORDER_WIDTH;
            this.bmd = this.make.bitmapData(this.game.width * 0.08 + 4 * border,
                this.game.height * 0.08 + 4 * border, "bmdHeroArrows", true);
            this.bmd.ctx.strokeStyle = this.strokeStyle;
            this.bmd.ctx.shadowColor = "#ffffff";
            this.bmd.ctx.lineWidth = BORDER_WIDTH;
            this.bmd.ctx.beginPath();
            this.bmd.ctx.moveTo(this.bmd.width * 0.1, this.bmd.height * 0.5);
            this.bmd.ctx.lineTo(this.bmd.width * 0.9, this.bmd.height * 0.1);
            this.bmd.ctx.lineTo(this.bmd.width * 0.9, this.bmd.height * 0.9);
            this.bmd.ctx.lineTo(this.bmd.width * 0.1, this.bmd.height * 0.5);
            this.bmd.ctx.stroke();
            this.bmd.ctx.closePath();

            let leftHeroArrow = this.game.add.sprite(this.game.width * 0.05, this.game.height * 0.2,
                this.game.cache.getBitmapData("bmdHeroArrows"));
            leftHeroArrow.anchor.set(0.5, 0.5);
            leftHeroArrow.inputEnabled = true;

            leftHeroArrow.events.onInputOver.add(() => {
                leftHeroArrow.tint = 0x6975FF;
            }, this);

            leftHeroArrow.events.onInputOut.add(() => {
                leftHeroArrow.tint = 0xffffff;
            }, this);

            leftHeroArrow.events.onInputDown.add(() => {
                this.soundController.buttonClickSound.play();

                this.changeHeroLeft();
            }, this);

            let rightHeroArrow = this.game.add.sprite(this.game.width * 0.45, this.game.height * 0.2,
                this.game.cache.getBitmapData("bmdHeroArrows"));
            rightHeroArrow.inputEnabled = true;
            rightHeroArrow.events.onInputOver.add(() => {

                rightHeroArrow.tint = 0x6975FF;
            }, this);

            rightHeroArrow.events.onInputOut.add(() => {
                rightHeroArrow.tint = 0xffffff;
            }, this);

            rightHeroArrow.events.onInputDown.add(() => {
                this.soundController.buttonClickSound.play();

                this.changeHeroRight();
            }, this);

            rightHeroArrow.anchor.set(0.5, 0.5);
            rightHeroArrow.scale.set(-1);
        }

        drawNameWindow() {
            this.bmd = this.make.bitmapData(this.game.width * 0.25, this.game.height * 0.08,
                "bmdNameRect", true);
            this.lineWidth = BORDER_WIDTH;
            this.bmd.ctx.lineWidth = this.lineWidth;
            this.bmd.ctx.strokeStyle = this.strokeStyle;
            this.bmd.ctx.strokeRect(this.lineWidth, this.lineWidth,
                this.bmd.width - 2 * this.lineWidth, this.bmd.height - 2 * this.lineWidth);
            let name = this.make.text(0, 0, "Hero name: ", this.style);
            name.anchor.set(0.5, 0.5);
            this.bmd.draw(name, this.bmd.width * 0.28, this.bmd.height * 0.54);

            this.nameWindow = this.game.add.sprite(this.game.width * 0.25, this.game.height * 0.4
                , this.game.cache.getBitmapData("bmdNameRect"));
            this.nameWindow.anchor.set(0.5, 0.5);

            this.nameGroup = this.game.add.group();
            if(this.heroSprite.animations.currentAnim ==
                this.heroSprite.animations.getAnimation("allMaleAnimations")){
                let heroName = this.add.text(this.nameWindow.x + this.nameWindow.width * 0.2,
                    this.nameWindow.y + this.nameWindow.height * 0.1, this.currentHero.maleName, this.style);
                heroName.anchor.set(0.5, 0.5);
                this.nameGroup.add(heroName);
            } else {
                let heroName = this.add.text(this.nameWindow.x + this.nameWindow.width * 0.2,
                    this.nameWindow.y + this.nameWindow.height * 0.1, this.currentHero.femaleName, this.style);
                heroName.anchor.set(0.5, 0.5);
                this.nameGroup.add(heroName);
            }
        }

        drawMaleButton() {
            let border = BORDER_WIDTH;
            this.bmd = this.make.bitmapData(this.game.width * 0.08 + 4 * border,
                this.game.height * 0.06 + 4 * border, "bmdMaleButton", true);
            this.bmd.ctx.strokeStyle = this.strokeStyle;
            this.bmd.ctx.lineWidth = BORDER_WIDTH;
            this.bmd.ctx.strokeRect(border, border,
                this.bmd.width - 2 * border, this.bmd.height - 2 * border);

            let maleText = this.make.text(0, 0, "Male", this.style);
            maleText.anchor.set(0.5, 0.5);
            this.bmd.draw(maleText, this.bmd.width * 0.5, this.bmd.height * 0.5);

            this.maleButton = this.game.add.sprite(this.game.width * 0.2, this.game.height * 0.5
                , this.game.cache.getBitmapData("bmdMaleButton"));
            this.maleButton.anchor.set(0.5, 0.5);
            this.maleButton.inputEnabled = true;

            this.maleButton.events.onInputOver.add(() => {
                this.maleButton.tint = 0x6975FF;
            }, this);

            this.maleButton.events.onInputOut.add(() => {
                this.maleButton.tint = 0xffffff;
            }, this);

            this.maleButton.events.onInputDown.add(() => {
                this.soundController.buttonClickSound.play();
                if (this.heroSprite.animations.currentAnim !==
                    this.heroSprite.animations.getAnimation("allMaleAnimations")) {
                    this.currentGenderFrames = "allMaleAnimations"
                    this.heroSprite.animations.play("allMaleAnimations");
                }
                this.nameGroup.destroy();
                this.drawNameWindow();
            }, this);
        }

        drawFemaleButton() {
            let border = BORDER_WIDTH;
            this.bmd = this.make.bitmapData(this.game.width * 0.08 + 4 * border,
                this.game.height * 0.06 + 4 * border, "bmdFemaleButton", true);
            this.bmd.ctx.strokeStyle = this.strokeStyle;
            this.bmd.ctx.lineWidth = BORDER_WIDTH;
            this.bmd.ctx.strokeRect(border, border,
                this.bmd.width - 2 * border, this.bmd.height - 2 * border);

            let femaleText = this.make.text(0, 0, "Female", this.style);
            femaleText.anchor.set(0.5, 0.5);
            this.bmd.draw(femaleText, this.bmd.width * 0.5, this.bmd.height * 0.5);

            this.femaleButton = this.game.add.sprite(this.game.width * 0.3, this.game.height * 0.5
                , this.game.cache.getBitmapData("bmdFemaleButton"));
            this.femaleButton.anchor.set(0.5, 0.5);
            this.femaleButton.inputEnabled = true;

            this.femaleButton.events.onInputOver.add(() => {
                this.femaleButton.tint = 0x6975FF;
            }, this);

            this.femaleButton.events.onInputOut.add(() => {
                this.femaleButton.tint = 0xffffff;
            }, this);

            this.femaleButton.events.onInputDown.add(() => {
                this.soundController.buttonClickSound.play();

                if (this.heroSprite.animations.currentAnim !==
                    this.heroSprite.animations.getAnimation("allFemaleAnimations")) {
                    this.currentGenderFrames = "allFemaleAnimations";
                    this.heroSprite.animations.play("allFemaleAnimations");
                }
                this.nameGroup.destroy();
                this.drawNameWindow();
            })
        }

        drawSelectButton() {
            let border = BORDER_WIDTH;
            this.bmd = this.make.bitmapData(this.game.width * 0.15 + 4 * border,
                this.game.height * 0.06 + 4 * border, "bmdSelectButton", true);
            this.bmd.ctx.strokeStyle = this.strokeStyle;
            this.bmd.ctx.lineWidth = BORDER_WIDTH;
            this.bmd.ctx.strokeRect(border, border,
                this.bmd.width - 2 * border, this.bmd.height - 2 * border);

            let selectButtonText = this.make.text(0, 0, "Select", this.style);
            selectButtonText.anchor.set(0.5, 0.5);
            this.bmd.draw(selectButtonText, this.bmd.width * 0.5, this.bmd.height * 0.5);


            let selectButton = this.game.add.sprite(this.game.width * 0.5, this.game.height * 0.5
                , this.game.cache.getBitmapData("bmdSelectButton"));
            selectButton.anchor.set(0.5, 0.5);
            selectButton.inputEnabled = true;

            this.playerArray = [];

            selectButton.events.onInputOver.add(() => {
                selectButton.tint = 0x6975FF;
            }, this);

            selectButton.events.onInputOut.add(() => {
                selectButton.tint = 0xffffff;
            }, this);

            selectButton.events.onInputDown.add(() => {
                this.soundController.buttonClickSound.play();
                // this.soundController.menuMusic.stop();

                this.playerArray.push(this.currentSelectedClass, this.currentSelectedGender);
                this.game.state.start("Game", true, false, this.playerArray);
            }, this)
        };


        drawStatsWindow() {
            this.bmd = this.make.bitmapData(this.game.width * 0.2, this.game.height * 0.4,
                "bmdStatsRect", true);
            this.lineWidth = BORDER_WIDTH;
            this.bmd.ctx.lineWidth = this.lineWidth;
            this.bmd.ctx.strokeStyle = this.strokeStyle;
            this.bmd.ctx.strokeRect(this.lineWidth, this.lineWidth,
                this.bmd.width - 2 * this.lineWidth, this.bmd.height - 2 * this.lineWidth);

            let statsWindow = this.game.add.sprite(this.game.width * 0.25, this.game.height * 0.76
                , this.game.cache.getBitmapData("bmdStatsRect"));
            statsWindow.anchor.set(0.5, 0.5);

            this.textGroup = this.game.add.group();
            let heroClass = this.make.text(0, 0, "Class: " + this.currentHero.heroClass, this.style);
            let heroHealth = this.make.text(0, 0, "Health: " + this.currentHero.health, this.style);
            let heroMana = this.make.text(0, 0, "Mana: " + this.currentHero.mana, this.style);
            let heroDamage = this.make.text(0, 0, "Damage: " + this.currentHero.heroDamage, this.style);
            let heroArmor = this.make.text(0, 0, "Armor: " + this.currentHero.armor, this.style);
            let heroStrength = this.make.text(0, 0, "Strength: " + this.currentHero.strength, this.style);
            let heroAgility = this.make.text(0, 0, "Agility: " + this.currentHero.agility, this.style);
            let heroIntellect = this.make.text(0, 0, "Intellect: " + this.currentHero.intellect, this.style);
            let heroMoveSpeed = this.make.text(0, 0, "Move speed: " + this.currentHero.moveSpeed, this.style);
            this.textGroup.addMultiple([heroClass, heroHealth, heroMana, heroDamage, heroArmor, heroStrength,
                heroAgility, heroIntellect, heroMoveSpeed]);
            this.textGroup.setAll("anchor", 0.5);

            let positionY = statsWindow.height + statsWindow.height * 0.4;
            for (let i = 0; i < this.textGroup.length; i++) {
                let positionX = statsWindow.width * 0.88;
                this.textGroup.children[i].position.set(positionX, positionY);
                positionY += 14;
            }
        }

        drawProsAndConsWindow() {
            this.bmd = this.make.bitmapData(this.game.width * 0.3, this.game.height * 0.5,
                "bmdProsAndConsRect", true);
            this.lineWidth = BORDER_WIDTH;
            this.bmd.ctx.lineWidth = this.lineWidth;
            this.bmd.ctx.strokeStyle = this.strokeStyle;
            this.bmd.ctx.strokeRect(this.lineWidth, this.lineWidth,
                this.bmd.width - 2 * this.lineWidth, this.bmd.height - 2 * this.lineWidth);

            this.prosAndConsGroup = this.game.add.group();

            let prosStyle = {font: "16px VT323", fill: "#56FF49", stroke: "#772A17", strokeThickness: 4};
            let pros = this.make.text(0, 0, "Pros: ", prosStyle);
            pros.anchor.set(0.5, 0.5);
            this.bmd.draw(pros, this.bmd.width * 0.15, this.bmd.height * 0.15);

            let prosText = this.make.text(0,0, this.currentHero.pros, this.style);

            let consStyle = {font: "16px VT323", fill: "#FF4B44", stroke: "#772A17", strokeThickness: 4};
            let cons = this.make.text(0, 0, "Cons: ", consStyle);
            cons.anchor.set(0.5, 0.5);
            this.bmd.draw(cons, this.bmd.width * 0.15, this.bmd.height * 0.6);

            let consText = this.make.text(0,0, this.currentHero.cons, this.style);

            let prosAndConsWindow = this.game.add.sprite(this.game.width * 0.77, this.game.height * 0.3
                , this.game.cache.getBitmapData("bmdProsAndConsRect"));
            prosAndConsWindow.anchor.set(0.5, 0.5);

            this.prosAndConsGroup.addMultiple([prosText,consText]);
            let positionY = prosAndConsWindow.position.y - prosAndConsWindow.height * 0.3;
            for (let i = 0; i < this.prosAndConsGroup.length; i++) {
                let positionX = prosAndConsWindow.position.x * 0.83;
                this.prosAndConsGroup.children[i].position.set(positionX, positionY);
                positionY += prosAndConsWindow.height * 0.45;
            }
        }

        drawStoryWindow() {
            this.bmd = this.make.bitmapData(this.game.width * 0.3, this.game.height * 0.4,
                "bmdStoryWindow", true);
            this.lineWidth = BORDER_WIDTH;
            this.bmd.ctx.lineWidth = this.lineWidth;
            this.bmd.ctx.strokeStyle = this.strokeStyle;
            this.bmd.ctx.strokeRect(this.lineWidth, this.lineWidth,
                this.bmd.width - 2 * this.lineWidth, this.bmd.height - 2 * this.lineWidth);

            this.storyWindow = this.game.add.sprite(this.game.width * 0.77, this.game.height * 0.75
                , this.game.cache.getBitmapData("bmdStoryWindow"));
            this.storyWindow.anchor.set(0.5, 0.5);

            let story = this.make.text(0, 0, "Story: ", this.style);
            story.anchor.set(0.5, 0.5);
            this.bmd.draw(story, this.storyWindow.width * 0.18, this.storyWindow.height * 0.15);

            let style = {font: "12px VT323", fill: "#E9802C", stroke: "#772A17", strokeThickness: 4};
            let storyText = this.game.make.text(0,0, this.currentHero.story, style);
            this.bmd.draw(storyText, this.storyWindow.width * 0.06, this.storyWindow.height * 0.25);
        }

        loadHeroInfo(): void {
            this.heroInfo = this.game.cache.getJSON(HERO_INFO_KEY);
        }

        createHeroArray(): void {
            this.heroArrayInfo = [];

            this.loadHeroInfo();
            for (let i = 0; i < this.heroInfo.heroes.length; i++) {
                this.heroArrayInfo.push(this.heroInfo.heroes[i]);
            }
        }

        changeHeroRight() {
            this.heroIndex += 1;
            if (this.heroIndex === 5) {
                this.heroIndex = 0;
            }
            this.currentHero = this.heroArrayInfo[this.heroIndex];
            this.heroSprite.kill();
            this.heroSprite.loadTexture(this.currentHero.heroClass);
            this.heroSprite.revive();
            this.heroSprite.animations.play(this.currentGenderFrames);
            this.textGroup.destroy();
            this.drawStatsWindow();
            this.nameGroup.destroy();
            this.drawNameWindow();
            this.prosAndConsGroup.destroy();
            this.drawProsAndConsWindow();
            this.storyWindow.destroy();
            this.drawStoryWindow();
        }

        changeHeroLeft() {
            this.heroIndex -= 1;
            if (this.heroIndex === -1) {
                this.heroIndex = 4;
            }
            this.currentHero = this.heroArrayInfo[this.heroIndex];
            this.heroSprite.kill();
            this.heroSprite.loadTexture(this.currentHero.heroClass);
            this.heroSprite.revive();
            this.heroSprite.animations.play(this.currentGenderFrames);
            this.textGroup.destroy();
            this.drawStatsWindow();
            this.nameGroup.destroy();
            this.drawNameWindow();
            this.prosAndConsGroup.destroy();
            this.drawProsAndConsWindow();
            this.storyWindow.destroy();
            this.drawStoryWindow();
        }

        updateSelection() {
            this.currentSelectedClass = this.currentHero.heroClass;
            if (this.currentGenderFrames === "allMaleAnimations") {
                this.currentSelectedGender = "male";
            } else {
                this.currentSelectedGender = "female";
            }
        }

        render() {
            // this.game.debug.body(this.leftHeroArrow);
        }
    }
}