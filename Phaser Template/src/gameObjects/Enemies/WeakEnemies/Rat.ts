module GameName {
    export class Rat extends WeakEnemy {

        constructor(game, x, y, enemyKey, experience, startingHealth: number, currentHealth: number, startingDamage: number,
                    startingArmor: number, startingMoveSpeed: number, visionRange: number, attackRange: number,
                    idleAnimationFrames: number[],gestureAnimationFrames: number[], moveAnimationFrames: number[],
                    attackAnimationFrames: number[], deathAnimationFrames: number[]) {
            super(game, x, y, enemyKey, experience, startingHealth, currentHealth, startingDamage, startingArmor, startingMoveSpeed,
                visionRange, attackRange, idleAnimationFrames, gestureAnimationFrames, moveAnimationFrames,
                attackAnimationFrames, deathAnimationFrames);

            this.initialize(RAT_AND_BAT_ENEMY_KEY, RAT_ENEMY_XP,RAT_ENEMY_STARTING_HEALTH, RAT_ENEMY_STARTING_HEALTH,
                RAT_ENEMY_STARTING_DAMAGE, RAT_ENEMY_STARTING_ARMOR, RAT_ENEMY_STARTING_MOVE_SPEED,
                RAT_ENEMY_VISION_RANGE, RAT_ENEMY_ATTACK_RANGE, RAT_IDLE_ANIMATION_FRAMES, RAT_MOVE_ANIMATION_FRAMES,
                RAT_ATTACK_ANIMATION_FRAMES, RAT_DEATH_ANIMATION_FRAMES, RAT_GESTURE_ANIMATION_FRAMES);

            this.enemyBody.body.setSize(21, 13, 4, 20);
        }
    }
}