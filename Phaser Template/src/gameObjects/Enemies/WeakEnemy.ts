module GameName {
    export class WeakEnemy extends Enemy {

        constructor(game, x, y, enemyKey: string, experience: number, startingHealth: number, currentHealth: number,
                    startingDamage: number, startingArmor: number, startingMoveSpeed: number, visionRange: number,
                    attackRange: number, idleAnimationFrames: number[], moveAnimationFrames: number[],
                    attackAnimationFrames: number[], deathAnimationFrames: number[], gestureAnimationFrames?: number[]) {
            super(game, x, y, enemyKey, experience, startingHealth, currentHealth, startingDamage, startingArmor, startingMoveSpeed,
                visionRange, attackRange, idleAnimationFrames, gestureAnimationFrames, moveAnimationFrames,
                attackAnimationFrames, deathAnimationFrames);

            this.initialize(enemyKey, experience, startingHealth, currentHealth, startingDamage, startingArmor, startingMoveSpeed,
                visionRange, attackRange, idleAnimationFrames, moveAnimationFrames, attackAnimationFrames,
                deathAnimationFrames, gestureAnimationFrames);
        }

        initialize(enemyKey: string, experience: number, startingHealth: number, currentHealth: number, startingDamage: number,
                   startingArmor: number, startingMoveSpeed: number, visionRange: number, attackRange: number,
                   idleAnimationFrames: number[], moveAnimationFrames: number[], attackAnimationFrames: number[],
                   deathAnimationFrames: number[], gestureAnimationFrames?: number[]) {
            this.currentState = EnemyStates.IDLE;

            this.enemyBody = this.game.add.sprite(this.x, this.y, enemyKey);
            this.enemyBody.animations.add("idle", idleAnimationFrames, 8, true);
            this.enemyBody.animations.add("gesture", gestureAnimationFrames, 8, true);
            this.enemyBody.animations.add("move", moveAnimationFrames, 20, true);
            this.enemyBody.animations.add("attack", attackAnimationFrames);
            this.enemyBody.animations.add("death", deathAnimationFrames, 5, false);
            this.enemyBody.anchor.set(0.5, 0.5);
            this.enemyBody.animations.play("idle");
            this.attackAnimationFrames = attackAnimationFrames;

            this.game.physics.arcade.enable(this.enemyBody);
            // this.enemyBody.inputEnabled = true;

            this.experience = experience;
            this.startingHealth = startingHealth;
            this.currentHealth = startingHealth;
            this.startingDamage = startingDamage;
            this.startingArmor = startingArmor;
            this.startingMoveSpeed = startingMoveSpeed;
            this.visionRange = visionRange;
            this.attackRange = attackRange;

            this.dealDamageSignal = new Phaser.Signal();
            this.attackAnimationSignal = new Phaser.Signal();
            this.moveSignal = new Phaser.Signal();
            this.experienceSignal = new Phaser.Signal();

            this.enemyVisionRange = this.enemyBody.body.width / 2 + this.visionRange / 2;
            this.enemyAttackRange = this.enemyBody.body.width / 2 + this.attackRange / 2;

            this.attackAnimationSignal.add((attackAvailable) => {
                if (attackAvailable) {
                    this.attackAnimationTimer = this.game.time.create();
                    this.attackAnimationTimer.loop(Phaser.Timer.SECOND * 1.5, this.attackHero, this);
                    this.attackAnimationTimer.start();
                }
            }, this);

            this.moveSignal.add((directionNormalized) => {
                this.enemyBody.body.velocity.x = directionNormalized.x * this.startingMoveSpeed;
                this.enemyBody.body.velocity.y = directionNormalized.y * this.startingMoveSpeed;
            }, this);

        }

        checkVisionRange(heroSprite: Phaser.Sprite) {
            super.checkVisionRange(heroSprite);

            if (this.isAlive) {
                let heroCenterX = (heroSprite.body.x + heroSprite.body.width / 2);
                let heroCenterY = (heroSprite.body.y + heroSprite.body.height / 2);
                let enemyCenterX = (this.enemyBody.body.x + this.enemyBody.body.width / 2);
                let enemyCenterY = (this.enemyBody.body.y + this.enemyBody.body.height / 2);

                this.distance = Phaser.Math.distance(heroCenterX, heroCenterY, enemyCenterX, enemyCenterY);
                this.directionNormalized = new Phaser.Point(heroCenterX - enemyCenterX,
                    heroCenterY - enemyCenterY).normalize();
                this.changeFacingDirection(heroSprite);

                if (this.distance < this.enemyVisionRange && this.distance > this.enemyAttackRange) {
                    this.changeToMoveState();
                    this.moveSignal.dispatch(this.directionNormalized);
                }
                else if (this.distance < this.enemyAttackRange) {
                    this.resetVelocity();
                    this.changeToAttackState();
                }
                else {
                    this.resetVelocity();
                    this.resetIdleAnimation();
                }
            }
            else {
                this.resetVelocity();
                this.onDeath();
            }

        }
    }
}