module GameName {

    const HERO_STARTING_STRENGTH: number = 12;
    const HERO_STARTING_AGILITY: number = 10;
    const HERO_STARTING_INTELLECT: number = 20;
    const HERO_STARTING_HEALTH: number = 60;
    const HERO_STARTING_MANA: number = 100;
    const HERO_STARTING_DAMAGE: number = 8;
    const HERO_STARTING_ARMOR: number = 8;
    const HERO_STARTING_MOVE_SPEED: number = 105;

    export class MageHero extends Hero {

        constructor(game: Phaser.Game, experience:number, strength: number, agility: number, intellect: number, startingHealth: number,
                    currentHealth: number, startingMana: number, currentMana: number, startingDamage: number,
                    startingArmor: number, startingMoveSpeed: number, keys, x: number, y: number, spriteKey: string,
                    attackRange?: number, idleAnimationFrames?: number[], gestureAnimationFrames?: number[],
                    moveAnimationFrames?: number[], attackAnimationFrames?: number[], deathAnimationFrames?: number[]) {
            super(game, x, y, experience, HERO_STARTING_STRENGTH, HERO_STARTING_AGILITY, HERO_STARTING_INTELLECT, HERO_STARTING_HEALTH, currentHealth, HERO_STARTING_MANA, currentMana,
                HERO_STARTING_DAMAGE, HERO_STARTING_ARMOR, HERO_STARTING_MOVE_SPEED, keys, spriteKey, attackRange, idleAnimationFrames,
                gestureAnimationFrames ,moveAnimationFrames, attackAnimationFrames, deathAnimationFrames);

            this.currentHealth=HERO_STARTING_HEALTH;
            this.currentMana=HERO_STARTING_MANA;
        }
    }
}