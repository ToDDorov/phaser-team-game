module GameName {
    export class LevelController {
        private game: Phaser.Game;

        public gameOverEvent: Phaser.Signal;

        private selectedHeroArray: Array<string>;

        private player: Hero;
        private enemy: Enemy;

        public playerHud: Phaser.Sprite;
        public healthBar: Phaser.Sprite;
        public manaBar: Phaser.Sprite;
        public xpBar: Phaser.Sprite;

        public healthText: Phaser.Text;
        public manaText: Phaser.Text;
        public xpText: Phaser.Text;
        public goldText: Phaser.Text;

        public hudUpdateSignal: Phaser.Signal;

        private currentLevel: Level;
        private startingTown: StartingTown;
        private levelOne: LevelOne;

        private interactButton: Phaser.Key;

        private heroFactory: HeroFactory;

        private soundController: SoundController;
        private npcController: NpcController;
        private enemyController: EnemyController;

        constructor(game, selectedHero: Array<string>) {
            this.game = game;

            this.gameOverEvent = new Phaser.Signal();

            this.interactButton = this.game.input.keyboard.addKey(Phaser.Keyboard.E);

            this.selectedHeroArray = [];
            this.selectedHeroArray = selectedHero;

            this.startingTown = new StartingTown(this.game);
            this.startingTown.create();

            this.levelOne = new LevelOne(this.game);

            this.currentLevel = this.startingTown;
            this.currentLevel.floor.resizeWorld();

            // this.levelOne = new LevelOne(this.game);
            // this.levelOne.create();
            // this.currentLevel = this.levelOne;

            this.heroFactory = new HeroFactory(this.game);

            let playerObject = this.currentLevel.getObjectFromTile("entities", "hero");
            this.player = this.heroFactory.CreateHero(this.selectedHeroArray[0], this.selectedHeroArray[1], playerObject.x,
                playerObject.y);

            this.currentLevel.group.addMultiple([this.player.heroBody, this.currentLevel.collisionBlocks,
                this.currentLevel.walls, this.currentLevel.decor, this.startingTown.npcGear.sprite,
                this.startingTown.npcGear.text, this.startingTown.npcSkills.sprite, this.startingTown.npcSkills.text]);

            this.game.camera.follow(this.player.heroBody);

            this.soundController = new SoundController(this.game);
            this.npcController = new NpcController(this.game, this.startingTown, this.player, this.soundController,
                this.interactButton, this.selectedHeroArray);
            this.enemyController = new EnemyController(this.game, this.startingTown, this.levelOne, this.player,
                this.interactButton, this.enemy, this.soundController);

            this.hudUpdateSignal = new Phaser.Signal();

            this.playerHud = this.game.add.sprite(0, 0, "playerHud");
            this.healthBar = this.game.add.sprite(this.playerHud.width * 0.42,this.playerHud.height * 0.11
                , "healthBar");
            this.manaBar = this.game.add.sprite(this.playerHud.width * 0.42,this.playerHud.height * 0.41,
                "manaBar");
            this.xpBar = this.game.add.sprite(this.playerHud.width * 0.42,this.playerHud.height * 0.71,
                "xpBar");

            this.playerHud.addChild(this.healthBar);
            this.playerHud.addChild(this.manaBar);
            this.playerHud.addChild(this.xpBar);

            let style = {font: "12px VT323", fill: "#fff", stroke: "#000", strokeThickness: 2};
            this.healthText = this.game.add.text(0,0, null, style);
            this.manaText = this.game.add.text(0,0, null, style);
            this.xpText = this.game.add.text(0,0, null, style);
            this.goldText = this.game.add.text(0,0, null,
                {font: "18px VT323", fill: "#FFE710", stroke: "#000", strokeThickness: 2});
        }

        create() {
            this.soundController.create();
            this.npcController.create();

            this.soundController.townMusic.loop = true;
            this.soundController.townMusic.play();
        }

        update() {
            this.game.physics.arcade.collide(this.player.heroBody, this.currentLevel.collisionBlocks);

            this.player.update();
            this.npcController.update();
            this.enemyController.update(this.currentLevel, this.levelOne.levelOneObjectFactory, this.player,
                this.player.heroBody.position);

            this.switchLevel();

            if(this.player.currentHealth <= 0) {
                this.gameOverEvent.dispatch();
            }

            this.updateHud();

            this.updateHealth();
            this.hudUpdateSignal.addOnce((cropHealth)=>{
                this.healthBar.crop(cropHealth, false);
            },this);
        }

        render() {
        }

        switchLevel(): void {
            if (this.startingTown.door !== null) {
                if (this.startingTown.door.intersects(this.player.heroBody.body, 0)) {
                    this.player.interactBool = true;

                    if (this.interactButton.isDown) {
                        this.player.interactBool = false;
                        this.soundController.townMusic.stop();
                        this.game.camera.fade(0x000, 500);
                        this.game.camera.onFadeComplete.add(this.changeLevel, this);
                    }
                }
            }
        }

        changeLevel(): void {
            this.startingTown.destroy();

            this.levelOne.create();

            this.currentLevel = this.levelOne;

            this.game.add.existing(this.levelOne.levelOneObjectFactory.minotaurStatue);

            this.levelOne.floor.resizeWorld();

            let playerPosition = this.currentLevel.getObjectFromTile("entities", "hero");
            this.player.heroBody.x = playerPosition.x;
            this.player.heroBody.y = playerPosition.y;

            this.enemyController.addEnemySignal(this.levelOne.levelOneObjectFactory);

            this.levelOne.levelOneObjectFactory.goldPickUpSignal.add((goldValue) => {
                this.player.gold += goldValue;
            }, this);

            this.levelOne.levelOneObjectFactory.potionPickUpSignal.add((healthPotionValue)=>{
                this.player.currentHealth += healthPotionValue;
            },this);

            this.game.world.bringToTop(this.player.rayCast);
            this.game.world.bringToTop(this.player.rangeGraphics);
            this.game.world.bringToTop(this.player.interactButtonSprite);
            this.game.world.bringToTop(this.playerHud);
            this.game.world.bringToTop(this.healthText);
            this.game.world.bringToTop(this.manaText);
            this.game.world.bringToTop(this.xpText);
            this.game.world.bringToTop(this.goldText);

            this.levelOne.levelOneObjectFactory.enemies.forEach((enemy) => {
                this.game.world.bringToTop(enemy.rangeGraphics);
                this.game.world.bringToTop(enemy.attackRangeGraphic);
                // this.game.world.bringToTop(enemy.damageText);
            }, null);

            this.currentLevel.group.addMultiple([this.levelOne.levelOneObjectFactory.enemies, this.currentLevel.decor,
                this.levelOne.levelOneObjectFactory.goldGroup, this.player.heroBody, this.currentLevel.collisionBlocks,
                this.currentLevel.walls, this.currentLevel.roof, this.levelOne.levelOneObjectFactory.torchGroup]);

            this.game.camera.resetFX();
            this.game.camera.shake(0.05, 650);

            this.soundController.levelOneMusic.loop = true;
            this.soundController.levelOneMusic.play();
        }

        updateHud() {
            this.playerHud.position.x = this.game.camera.position.x + this.game.camera.width * 0.03;
            this.playerHud.position.y = this.game.camera.position.y + this.game.camera.height * 0.03;

            this.healthText.position.set(this.playerHud.position.x + this.playerHud.width * 0.45,
                this.playerHud.position.y + this.playerHud.height * 0.06);
            this.healthText.text = "health: " + Math.floor(this.player.currentHealth).toString() + "/" +
                this.player.startingHealth.toString();
            this.manaText.position.set(this.playerHud.position.x + this.playerHud.width * 0.45,
                this.playerHud.position.y + this.playerHud.height * 0.36);
            this.manaText.text = "mana: " + Math.floor(this.player.currentMana).toString() + "/" +
                this.player.startingMana.toString();
            this.xpText.position.set(this.playerHud.position.x + this.playerHud.width * 0.42,
                this.playerHud.position.y + this.playerHud.height * 0.68);
            this.xpText.text = "xp: " + Math.floor(this.player.experience).toString() + "/" +
                this.player.experienceNeededForLevel.toString() + " level: " + this.player.level.toString();
            this.goldText.position.set(this.playerHud.position.x + this.playerHud.width * 0.42,
                this.playerHud.position.y + this.playerHud.height * 0.9);
            this.goldText.text = "Gold: " + this.player.gold.toString();


            // let cropHealth = new Phaser.Rectangle(0, 0, this.healthBar.width, this.healthBar.height);
            // cropHealth.width = cropHealth.width * (this.player.currentHealth / this.player.startingHealth);
            // console.log(cropHealth.width);
            // this.healthBar.crop(cropHealth, false);
            // // this.healthBar.width = this.healthBar.width * (this.player.currentHealth / this.player.startingHealth);
            // this.manaBar.width = this.manaBar.width * (this.player.currentMana / this.player.startingMana);
            // this.xpBar.width = this.xpBar.width * (this.player.experience / this.player.experienceNeededForLevel);

        }

        updateHealth(){
            if(this.player.isDamageTaken){
                let cropHealth = new Phaser.Rectangle(0, 0, this.healthBar.width, this.healthBar.height);
                cropHealth.width = cropHealth.width * (this.player.currentHealth / this.player.startingHealth);
                this.hudUpdateSignal.dispatch(cropHealth);
            } else {
                this.hudUpdateSignal.forget();
            }
        }

        destroy() {
            this.player.destroy();
            this.player = null;
            this.startingTown.destroy();
            this.startingTown = null;

            this.heroFactory = null;
            this.soundController = null;
            this.enemyController = null;
            this.interactButton = null
        }
    }
}