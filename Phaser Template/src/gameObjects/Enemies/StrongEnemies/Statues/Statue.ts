module GameName {
    export class Statue extends Enemy {

        constructor(game, x, y, enemyKey, experience, startingHealth: number, currentHealth: number, startingDamage: number,
                    startingArmor: number, startingMoveSpeed: number, visionRange: number, attackRange: number,
                    idleAnimationFrames: number[],gestureAnimationFrames: number[], moveAnimationFrames: number[],
                    attackAnimationFrames: number[], deathAnimationFrames: number[]) {
            super(game, x, y, enemyKey, experience, startingHealth, currentHealth, startingDamage, startingArmor, startingMoveSpeed,
                visionRange, attackRange, idleAnimationFrames, gestureAnimationFrames, moveAnimationFrames,
                attackAnimationFrames, deathAnimationFrames);

            // this.initialize(STATUE_ENEMY_KEY, STATUE_ENEMY_XP, STATUE_ENEMY_STARTING_HEALTH, MOSS_STATUE_ENEMY_STARTING_HEALTH,
            //     STATUE_ENEMY_STARTING_DAMAGE, STATUE_ENEMY_STARTING_ARMOR, STATUE_ENEMY_STARTING_MOVE_SPEED,
            //     STATUE_ENEMY_VISION_RANGE, STATUE_ENEMY_ATTACK_RANGE, STATUE_IDLE_ANIMATION_FRAMES,
            //     STATUE_MOVE_ANIMATION_FRAMES, STATUE_ATTACK_ANIMATION_FRAMES, STATUE_DEATH_ANIMATION_FRAMES);

            this.enemyBody.body.setSize(50, 60, 10, 5);
            this.enemyBody.scale.set(-1, 1);
            this.defaultFaceDirection = 1;
        }
    }
}