/// <reference path = "lib/phaser.d.ts"/>
// CHANGE: to your game name
module GameName {
    // CHANGE: to your game name
    class GameName extends Phaser.Game {
        constructor(width?:number, height?:number) {
            // Creates the game
            super(width, height, Phaser.CANVAS, 'phaser-div');
            this.state.add("Boot", Boot, false);
            this.state.add("Preloader", Preloader, false);
            this.state.add("Menu", Menu, false);
            this.state.add("SelectHero", SelectHero, false);
            this.state.add("Game", Game, false);
            this.state.add("GameOver", GameOver, false);

            this.state.start("Boot");
        }
    }

    window.onload = ()=> {
        // CHANGE: to your game name
        new GameName(640, 360);
    }
}