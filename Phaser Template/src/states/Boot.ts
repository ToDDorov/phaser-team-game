/// <reference path = "../../lib/phaser.d.ts"/>

module GameName {
    export class Boot extends Phaser.State {
        background:Phaser.Image;

        preload() {
            //PRELOAD ALL YOUR GAME ASSETS HERE, e.g.
            this.game.load.image("background","src/assets/graphics/splashCat.jpg");
        }

        create() {
            // CREATE PRELOAD CONTENT
            this.game.scale.scaleMode = Phaser.ScaleManager.RESIZE;
            this.game.physics.startSystem(Phaser.Physics.ARCADE);
            this.game.renderer.renderSession.roundPixels = true;
            Phaser.Canvas.setImageRenderingCrisp(this.game.canvas);

            this.game.state.start("Preloader");
            this.game.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
            this.background = this.game.add.image(this.game.width/2, this.game.height/2, "background");
            this.background.anchor.set(0.5,0.5);
            this.background.scale.set(0.67,0.67);
            this.background.alpha = 0;
            this.game.add.tween(this.background).to({alpha:1},2000, Phaser.Easing.Default ,
                true,0 , 0,true)
                .onComplete.add(() => {
                    this.game.state.start("Preloader");
            });
        }
    }
}