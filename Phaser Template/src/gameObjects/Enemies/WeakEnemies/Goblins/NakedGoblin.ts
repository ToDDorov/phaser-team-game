module GameName {
    export class NakedGoblin extends WeakEnemy {

        constructor(game, x, y, enemyKey, experience, startingHealth: number, currentHealth: number, startingDamage: number,
                    startingArmor: number, startingMoveSpeed: number, visionRange: number, attackRange: number,
                    idleAnimationFrames: number[],gestureAnimationFrames: number[], moveAnimationFrames: number[],
                    attackAnimationFrames: number[], deathAnimationFrames: number[]) {
            super(game, x, y, enemyKey, experience, startingHealth, currentHealth, startingDamage, startingArmor, startingMoveSpeed,
                visionRange, attackRange, idleAnimationFrames, gestureAnimationFrames, moveAnimationFrames,
                attackAnimationFrames, deathAnimationFrames);

            this.initialize(GOBLINS_ENEMY_KEY, NAKED_GOBLIN_ENEMY_XP, NAKED_GOBLIN_ENEMY_STARTING_HEALTH,
                NAKED_GOBLIN_ENEMY_STARTING_HEALTH, NAKED_GOBLIN_ENEMY_STARTING_DAMAGE,
                NAKED_GOBLIN_ENEMY_STARTING_ARMOR, NAKED_GOBLIN_ENEMY_STARTING_MOVE_SPEED,
                NAKED_GOBLIN_ENEMY_VISION_RANGE, NAKED_GOBLIN_ENEMY_ATTACK_RANGE,
                NAKED_GOBLIN_IDLE_ANIMATION_FRAMES, NAKED_GOBLIN_MOVE_ANIMATION_FRAMES,
                NAKED_GOBLIN_ATTACK_ANIMATION_FRAMES, NAKED_GOBLIN_DEATH_ANIMATION_FRAMES,
                NAKED_GOBLIN_GESTURE_ANIMATION_FRAMES);

            this.enemyBody.body.setSize(18, 25, 9, 9);
            this.enemyBody.scale.set(-1, 1);
            this.defaultFaceDirection = 1;
        }
    }
}