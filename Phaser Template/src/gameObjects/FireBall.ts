module GameName{
    export class FireBall extends Bullet{
        constructor(game:Phaser.Game, x:number, y:number, width:number, height:number, angle:number, key:string){
            super(game, x, y, width, height, angle, FIREBALL_KEY);
        }
    }
}