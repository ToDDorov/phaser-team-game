/// <reference path = "../../lib/phaser.d.ts"/>
///<reference path="../utils/Constants.ts"/>

module GameName {
    export class Preloader extends Phaser.State {

        preload() {
            this.game.time.advancedTiming = true;
            this.load.tilemap(TOWN_TILEMAP_KEY, "src/assets/tilemaps/maps/town.json",
                null, Phaser.Tilemap.TILED_JSON);
            this.load.tilemap(LEVEL_ONE_TILEMAP_KEY, "src/assets/tilemaps/maps/newLevelOne.json",
                null, Phaser.Tilemap.TILED_JSON);
            this.load.image(TOWN_TILES_KEY, "src/assets/tilemaps/tiles/Castle2.png");
            this.load.image(LEVEL_ONE_TILES_KEY, "src/assets/tilemaps/tiles/tiles.png");

            this.load.spritesheet(ITEMS_SHEET_KEY, "src/assets/graphics/itemsSheet.png", 16,
                16, 8 * 9);
            this.game.load.json(ITEMS_INFO_KEY, 'src/assets/itemsInfo.json');
            this.game.load.json(HERO_INFO_KEY, "src/assets/heroInfo.json");
            this.load.spritesheet(INTERACT_BUTTON_KEY, "src/assets/graphics/interactButton.png", 16 ,
                16);
            this.load.image("playerHud", "src/assets/graphics/playerHud.png");
            this.load.image("healthBar", "src/assets/graphics/healthBar.png");
            this.load.image("manaBar", "src/assets/graphics/manaBar.png");
            this.load.image("xpBar", "src/assets/graphics/xpBar.png");

            // Heroes
            this.load.spritesheet(WARRIOR_HERO_KEY, "src/assets/Sprites/Heroes/warriorHero.png",
                32, 32);
            this.load.spritesheet(CLERIC_HERO_KEY, "src/assets/Sprites/Heroes/clericHero.png", 32,
                32);
            this.load.spritesheet(ROGUE_HERO_KEY, "src/assets/Sprites/Heroes/rogueHero.png", 32,
                32);
            this.load.spritesheet(MAGE_HERO_KEY, "src/assets/Sprites/Heroes/mageHero.png", 32,
                32);
            this.load.spritesheet(RANGER_HERO_KEY, "src/assets/Sprites/Heroes/rangerHero.png", 32,
                32);
            this.load.image(ARROW_KEY,"src/assets/graphics/arrow.png");
            this.load.image(FIREBALL_KEY,"src/assets/graphics/fireball.png");


            // Enemies
            this.load.spritesheet(RAT_AND_BAT_ENEMY_KEY, "src/assets/Sprites/Enemies/ratAndBat.png",
                32, 32);
            this.load.spritesheet(GOBLINS_ENEMY_KEY, "src/assets/Sprites/Enemies/goblin.png", 32,
                32);
            this.load.spritesheet(ORCS_ENEMY_KEY, "src/assets/Sprites/Enemies/orcWizard.png", 32,
                32);
            this.load.spritesheet(STATUE_ENEMY_KEY, "src/assets/Sprites/Enemies/statue.png", 64,
                64);
            this.load.spritesheet(MOSS_STATUE_ENEMY_KEY, "src/assets/Sprites/Enemies/statueMoss.png", 64,
                64);
            this.load.spritesheet(MINOTAUR_ENEMY_KEY, "src/assets/Sprites/Enemies/minotaur.png", 48,
                48);
            this.load.spritesheet(SNAKE_ENEMY_KEY, "src/assets/Sprites/Enemies/snake.png", 32,
                32);
            this.load.spritesheet(SLIMES_ENEMY_KEY, "src/assets/Sprites/Enemies/slimes.png", 32,
                32);
            this.load.spritesheet(SKELETON_ENEMY_KEY, "src/assets/Sprites/Enemies/skeleton.png", 32,
                32);

            // NPCs
            this.load.atlas(NPC_ATLAS_KEY, "src/assets/Sprites/NPC/npcAtlas.png",
                'src/assets/Sprites/NPC/atlas.json');

            //Menu
            this.load.image("backgroundMenu", "src/assets/graphics/backgroundMenu.jpg");
            this.load.image("buttonPlay", "src/assets/graphics/buttonPlay.png");
            this.load.image("buttonOptions", "src/assets/graphics/buttonOptions.png");
            this.load.image("buttonExit", "src/assets/graphics/buttonExit.png");

            // Audio
            this.load.audio('menuClick', "src/assets/sounds/Menu Selection Click.wav");
            this.load.audio('menuMusic', "src/assets/sounds/menu.ogg");
            this.load.audio('townMusic', "src/assets/sounds/rpg_village02__loop.ogg");
            this.load.audio("levelOneMusic", "src/assets/sounds/levelOne/dungeonSound.wav");
            this.load.audio("npcGearWelcome", "src/assets/sounds/npcGear/welcome.wav");
            this.load.audio("npcGearBuyin", "src/assets/sounds/npcGear/whatyaBuyin.wav");
            this.load.audio("npcGearLeaving", "src/assets/sounds/npcGear/leaving.wav");
            this.load.audio("npcGearWiseChoice", "src/assets/sounds/npcGear/wiseChoice.wav");
            this.load.audio("npcSkillWelcome", "src/assets/sounds/npcSkill/magicPowers.mp3");
            this.load.audio("goldPickup", "src/assets/sounds/levelOne/goldPickup.wav");
            this.load.audio("healthPotionSound", "src/assets/sounds/levelOne/healthPotion.wav");
            this.load.audio("levelup", "src/assets/sounds/levelup.wav");
            this.load.audio("chest", "src/assets/sounds/levelOne/chest.wav");
            this.load.audio("batSound", "src/assets/sounds/levelOne/bat.wav");
            this.load.audio("trapSound", "src/assets/sounds/levelOne/trap.wav");
            this.load.audio("gemSound", "src/assets/sounds/levelOne/gem.wav");
            this.load.audio("leverSound", "src/assets/sounds/levelOne/lever.wav");

            this.load.image("gearShop", "src/assets/graphics/gearShop.png");
            this.load.image("skillShop", "src/assets/graphics/skillShop.png");
            this.load.image("inventory", "src/assets/graphics/inventory.png");
            this.load.image("clicker", "src/assets/graphics/clicker.png");

            this.load.image("selectScreenBackground", "src/assets/graphics/selectScreenBackground.png");
            this.load.image("gameOver", "src/assets/graphics/gameOver.jpg");

            // Misc
            this.load.image("minotaurStatue", "src/assets/graphics/statueMinotaur.png");
            this.load.image("snakeStatue", "src/assets/graphics/statueSnake.png");
            this.load.image("goldBig", "src/assets/graphics/goldBig.png");
            this.load.spritesheet("chest", "src/assets/graphics/chest.png", 16, 16);
            this.load.spritesheet("lever", "src/assets/graphics/lever.png", 15.5, 11);
            this.load.image("gemGreen", "src/assets/graphics/gemGreen.png");
            this.load.spritesheet("torch", "src/assets/graphics/torch.png", 4, 12);
            this.load.image("healthPotion", "src/assets/graphics/healthPotion.png");
            this.load.spritesheet("fire", "src/assets/graphics/fire.png", 12,14);

            //bug fixer
            this.load.spritesheet("emptySprite", "src/assets/Sprites/Enemies/emptySprite.png", 32,
                32);
        }

        create() {
            this.game.state.start("Menu");
        }
    }
}