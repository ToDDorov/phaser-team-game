module GameName {
    export class Entity extends Phaser.Sprite {

        protected facingRight: number;
        protected defaultFaceDirection: number;
        protected experience: number;

        public isDamageTaken: boolean;

        public isAlive: boolean;
        public dealDamageSignal: Phaser.Signal;
        public startingHealth: number;
        public currentHealth: number;
        public startingDamage: number;
        public startingMoveSpeed: number;
        public startingArmor: number;
        public rayCast: Phaser.BitmapData;
        public rangeGraphics: Phaser.Graphics;
        public attackRangeGraphic: Phaser.Graphics;

        constructor(game, x, y, experience?: number, startingHealth?: number, currentHealth?: number, startingDamage?: number,
                    startingArmor?: number, startingMoveSpeed?: number, visionRange?: number, attackRange?: number,
                    idleAnimationFrames?: number[], gestureAnimationFrames?: number[], moveAnimationFrames?: number[],
                    attackAnimationFrames?: number[], deathAnimationFrames?: number[]) {
            super(game, x, y)

            this.experience = experience;
            this.startingHealth = startingHealth;
            this.currentHealth = startingHealth;
            this.startingDamage = startingDamage;
            this.startingArmor = startingArmor;
            this.startingMoveSpeed = startingMoveSpeed;
            this.isAlive = true;
            this.rayCast = this.game.add.bitmapData(this.game.width, this.game.height);
            this.rayCast.context.fillStyle = 'rgb(255, 255, 255)';
            this.rayCast.context.strokeStyle = 'rgb(255, 255, 255)';
            this.game.add.image(0, 0, this.rayCast);

            this.rangeGraphics = this.game.add.graphics(0, 0);
            this.attackRangeGraphic = this.game.add.graphics(0, 0);

            this.game.physics.enable(this, Phaser.Physics.ARCADE);
        }

        drawRange(spriteBodyX: number, spriteBodyY: number, radius: number, color?: number) {
            this.rangeGraphics.clear();
            this.rangeGraphics.lineStyle(2, 0xFFF8C5, 0.5);
            this.rangeGraphics.drawCircle(spriteBodyX, spriteBodyY,
                radius);
        }

        drawAttackRange(spriteBodyX: number, spriteBodyY: number, radius: number, color?: number) {
            this.attackRangeGraphic.clear();
            this.attackRangeGraphic.lineStyle(2, color, 0.5);
            this.attackRangeGraphic.drawCircle(spriteBodyX, spriteBodyY,
                radius);
        }

        takeDamage(damageTaken: number): boolean {
            let damageMultiplier = damageTaken / (damageTaken + this.startingArmor);
            let finalDamage = damageTaken * damageMultiplier;
            this.currentHealth -= finalDamage;
            this.isDamageTaken = true;
            return this.currentHealth <= 0;
        }



    }
}