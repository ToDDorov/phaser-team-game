module GameName {
    export class npcGear extends NPC {

        constructor(game, x, y) {
            super(game, x, y);

            this.sprite = this.game.add.sprite(x, y, NPC_ATLAS_KEY);
            this.sprite.animations.add('idle', ['oldman-idle-1', 'oldman-idle-2', 'oldman-idle-3',
                'oldman-idle-4', 'oldman-idle-5', 'oldman-idle-6', 'oldman-idle-7', 'oldman-idle-8',
                'oldman-idle-6', 'oldman-idle-5', 'oldman-idle-4', 'oldman-idle-3'], 7, true);
            this.sprite.animations.play("idle");
            this.sprite.scale.setTo(0.8);
            this.sprite.anchor.set(0.5, 0.5);
            let style = {font: "10px Arial", fill: "#e9e9ff", align: "center", stroke: '#000', strokeThickness: 3};
            this.text = this.game.add.text(x - 50, y - 36, NPC_SKILL_TEXT_SPEECH, style);
            this.text.alpha = 0;
            this.game.physics.arcade.enable(this.sprite);
            this.sprite.body.immovable = true;
        }
    }
}