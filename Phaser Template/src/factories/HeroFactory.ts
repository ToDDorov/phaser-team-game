module GameName {
    export class HeroFactory {
        private game: Phaser.Game;

        constructor(game) {
            this.game = game;
        }

        CreateHero(hero: string, gender: string, x: any, y: any) {
            if (hero === "Warrior" && gender === "male") {
                return new WarriorHero(this.game, STARTING_EXPERIENCE, null, null, null,
                    null, null, null, null, null, null, null,
                    PLAYER_CONTROL, x, y, hero, MELEE_RADIUS, MALE_IDLE_ANIMATION_FRAMES,
                    MALE_GESTURE_ANIMATION_FRAMES, MALE_MOVE_ANIMATION_FRAMES,
                    MALE_ATTACK_ANIMATION_FRAMES, MALE_DEATH_ANIMATION_FRAMES);

            } else if (gender === "female") {
                return new WarriorHero(this.game, STARTING_EXPERIENCE, null, null, null,
                    null, null, null, null, null, null, null,
                    PLAYER_CONTROL, x, y, hero, MELEE_RADIUS, FEMALE_IDLE_ANIMATION_FRAMES,
                    FEMALE_GESTURE_ANIMATION_FRAMES, FEMALE_MOVE_ANIMATION_FRAMES,
                    FEMALE_ATTACK_ANIMATION_FRAMES, FEMALE_DEATH_ANIMATION_FRAMES);
            }

            if (hero === "Rogue" && gender === "male") {
                return new RogueHero(this.game, STARTING_EXPERIENCE, null, null, null,
                    null, null, null, null, null, null, null,
                    PLAYER_CONTROL, x, y, ROGUE_HERO_KEY, MELEE_RADIUS, MALE_IDLE_ANIMATION_FRAMES,
                    MALE_GESTURE_ANIMATION_FRAMES, MALE_MOVE_ANIMATION_FRAMES,
                    MALE_ATTACK_ANIMATION_FRAMES, MALE_DEATH_ANIMATION_FRAMES);
            } else if (gender === "female") {
                return new RogueHero(this.game, STARTING_EXPERIENCE, null, null, null,
                    null, null, null, null, null, null, null,
                    PLAYER_CONTROL, x, y, ROGUE_HERO_KEY, MELEE_RADIUS, FEMALE_IDLE_ANIMATION_FRAMES,
                    FEMALE_GESTURE_ANIMATION_FRAMES, FEMALE_MOVE_ANIMATION_FRAMES,
                    FEMALE_ATTACK_ANIMATION_FRAMES, FEMALE_DEATH_ANIMATION_FRAMES);
            }

            if (hero === "Ranger" && gender === "male") {
                return new RangerHero(this.game, STARTING_EXPERIENCE, null, null, null,
                    null, null, null, null, null, null, null,
                    PLAYER_CONTROL, x, y, RANGER_HERO_KEY, RANGE_RADIUS, MALE_IDLE_ANIMATION_FRAMES,
                    MALE_GESTURE_ANIMATION_FRAMES, MALE_MOVE_ANIMATION_FRAMES,
                    MALE_ATTACK_ANIMATION_FRAMES, MALE_DEATH_ANIMATION_FRAMES);
            } else if (gender === "female") {
                return new RangerHero(this.game, STARTING_EXPERIENCE, null, null, null,
                    null, null, null, null, null, null, null,
                    PLAYER_CONTROL, x, y, RANGER_HERO_KEY, RANGE_RADIUS, FEMALE_IDLE_ANIMATION_FRAMES,
                    FEMALE_GESTURE_ANIMATION_FRAMES, FEMALE_MOVE_ANIMATION_FRAMES,
                    FEMALE_ATTACK_ANIMATION_FRAMES, FEMALE_DEATH_ANIMATION_FRAMES);
            }

            if (hero === "Mage" && gender === "male") {
                return new MageHero(this.game, STARTING_EXPERIENCE, null, null, null,
                    null, null, null, null, null, null, null,
                    PLAYER_CONTROL, x, y, MAGE_HERO_KEY, RANGE_RADIUS, MALE_IDLE_ANIMATION_FRAMES,
                    MALE_GESTURE_ANIMATION_FRAMES, MALE_MOVE_ANIMATION_FRAMES,
                    MALE_ATTACK_ANIMATION_FRAMES, MALE_DEATH_ANIMATION_FRAMES);
            } else if (gender === "female") {
                return new MageHero(this.game, STARTING_EXPERIENCE, null, null, null,
                    null, null, null, null, null, null, null,
                    PLAYER_CONTROL, x, y, MAGE_HERO_KEY, RANGE_RADIUS, FEMALE_IDLE_ANIMATION_FRAMES,
                    FEMALE_GESTURE_ANIMATION_FRAMES, FEMALE_MOVE_ANIMATION_FRAMES,
                    FEMALE_ATTACK_ANIMATION_FRAMES, FEMALE_DEATH_ANIMATION_FRAMES);
            }

            if (hero === "Cleric" && gender === "male") {
                return new ClericHero(this.game, STARTING_EXPERIENCE, null, null, null,
                    null, null, null, null, null, null, null,
                    PLAYER_CONTROL, x, y, CLERIC_HERO_KEY, RANGE_RADIUS, MALE_IDLE_ANIMATION_FRAMES,
                    MALE_GESTURE_ANIMATION_FRAMES, MALE_MOVE_ANIMATION_FRAMES,
                    MALE_ATTACK_ANIMATION_FRAMES, MALE_DEATH_ANIMATION_FRAMES);

            } else if (gender === "female") {
                return new ClericHero(this.game, STARTING_EXPERIENCE, null, null, null,
                    null, null, null, null, null, null, null,
                    PLAYER_CONTROL, x, y, CLERIC_HERO_KEY, RANGE_RADIUS, FEMALE_IDLE_ANIMATION_FRAMES,
                    FEMALE_GESTURE_ANIMATION_FRAMES, FEMALE_MOVE_ANIMATION_FRAMES,
                    FEMALE_ATTACK_ANIMATION_FRAMES, FEMALE_DEATH_ANIMATION_FRAMES);
            }
        }
    }
}