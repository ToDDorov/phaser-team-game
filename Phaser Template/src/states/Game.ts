///<reference path="../levels/Level.ts"/>
///<reference path="../levels/StartingTown.ts"/>
///<reference path="../utils/Constants.ts"/>

module GameName {

    export class Game extends Phaser.State {
        game: Phaser.Game;
        selectedHeroArray: Array<string>;
        levelController: LevelController;

        init(playerArray: string[]) {
            this.selectedHeroArray = [];
            this.selectedHeroArray = playerArray;
        }

        create() {
            this.levelController = new LevelController(this.game, this.selectedHeroArray);
            this.levelController.create();

            this.levelController.gameOverEvent.add(() => {
                this.game.state.start("GameOver");
            }, this);
        }

        update() {
            this.levelController.update();
        }

        render() {
            this.levelController.render();
        }

        shutdown() {
            this.levelController.destroy();
            this.levelController = null;

            for(let i: number = 0; i< this.selectedHeroArray.length; i++) {
                this.selectedHeroArray[i] = null;
            }
            this.selectedHeroArray = [];
            this.selectedHeroArray = null;
        }
    }
}