module GameName {
    export class CapedOrc extends WeakEnemy {

        constructor(game, x, y, enemyKey, experience, startingHealth: number, currentHealth: number, startingDamage: number,
                    startingArmor: number, startingMoveSpeed: number, visionRange: number, attackRange: number,
                    idleAnimationFrames: number[],gestureAnimationFrames: number[], moveAnimationFrames: number[],
                    attackAnimationFrames: number[], deathAnimationFrames: number[]) {
            super(game, x, y, enemyKey, experience, startingHealth, currentHealth, startingDamage, startingArmor, startingMoveSpeed,
                visionRange, attackRange, idleAnimationFrames, gestureAnimationFrames, moveAnimationFrames,
                attackAnimationFrames, deathAnimationFrames);

            this.initialize(ORCS_ENEMY_KEY, CAPED_ORC_ENEMY_XP, CAPED_ORC_ENEMY_STARTING_HEALTH, CAPED_ORC_ENEMY_STARTING_HEALTH,
                CAPED_ORC_ENEMY_STARTING_DAMAGE, CAPED_ORC_ENEMY_STARTING_ARMOR, CAPED_ORC_ENEMY_STARTING_MOVE_SPEED,
                CAPED_ORC_ENEMY_VISION_RANGE, CAPED_ORC_ENEMY_ATTACK_RANGE, CAPED_ORC_IDLE_ANIMATION_FRAMES,
                CAPED_ORC_MOVE_ANIMATION_FRAMES, CAPED_ORC_ATTACK_ANIMATION_FRAMES, CAPED_ORC_DEATH_ANIMATION_FRAMES,
                CAPED_ORC_GESTURE_ANIMATION_FRAMES);

            this.enemyBody.body.setSize(30, 32);
            this.enemyBody.scale.set(-1, 1);
            this.defaultFaceDirection = 1;
        }
    }
}