module GameName {
    export class EnemyController {
        private levelOne: LevelOne;
        private currentLevel: Level;
        private player: Hero;
        private interactButton: Phaser.Key;
        private enemy: Enemy;
        private startingTown: StartingTown;
        private soundController: SoundController;

        game: Phaser.Game;

        constructor(game, startingTown: StartingTown, levelOne: LevelOne, player: Hero,
                    interactButton: Phaser.Key, enemy: Enemy, soundController: SoundController) {
            this.game = game;
            this.startingTown = startingTown;
            this.levelOne = levelOne;
            this.player = player;
            this.interactButton = interactButton;
            this.enemy = enemy;
            this.soundController = soundController;
        }

        update(currentLevel: Level, levelOneObjectFactory: LevelOneObjectFactory, player: Hero,
               playerPosition: Phaser.Point) {
            this.currentLevel = currentLevel;
            if (this.currentLevel === this.levelOne) {
                this.levelOne.update(playerPosition);
                this.game.physics.arcade.collide(this.player.heroBody, levelOneObjectFactory.minotaurStatue);

                levelOneObjectFactory.pickGoldUp(this.player.heroBody);
                levelOneObjectFactory.pickPotionUp(this.player.heroBody);
                levelOneObjectFactory.pickGemUp(this.player.heroBody);

                levelOneObjectFactory.intersectsCowBox(this.player.heroBody, this.interactButton, this.player);
                levelOneObjectFactory.openChest(this.player.heroBody, this.interactButton, this.player);
                levelOneObjectFactory.activateLever(this.player.heroBody, this.interactButton, this.player);
                levelOneObjectFactory.intersectsExitBox(this.player.heroBody, this.interactButton, this.player);

                levelOneObjectFactory.chestGroup.forEach((chest) => {
                    this.game.physics.arcade.collide(this.player.heroBody, chest);
                }, this);

                levelOneObjectFactory.leverGroup.forEach((lever) => {
                    this.game.physics.arcade.collide(this.player.heroBody, lever);
                }, this);


                levelOneObjectFactory.enemies.forEach((enemy) => {
                    let inRange = Phaser.Math.distance(this.player.heroBody.x, this.player.heroBody.y,
                        enemy.enemyBody.x, enemy.enemyBody.y);
                    if (inRange <= this.player.attackRange && this.player.currentState == HeroStates.ATTACK) {
                        if (this.player.canHit) {
                            this.player.dealDamageSignal.addOnce((damage) => {
                                enemy.takeDamage(damage);
                                let isEnemyDead = enemy.takeDamage(damage);
                                if (isEnemyDead) {
                                    enemy.isAlive = false;
                                    enemy.experienceSignal.addOnce((experience) => {
                                        this.player.experience += experience;
                                    }, this);
                                }
                            }, this);
                            this.player.canHit = false;
                        }
                    }
                }, this);


                levelOneObjectFactory.enemies.forEach((enemy) => {
                    this.game.physics.arcade.collide(this.player.heroBody, enemy.enemyBody);
                    this.game.physics.arcade.collide(this.currentLevel.collisionBlocks, enemy.enemyBody);
                    levelOneObjectFactory.enemies.forEach((enemy2) => {
                        this.game.physics.arcade.collide(enemy.enemyBody, enemy2.enemyBody);
                    }, this);
                    enemy.update();
                    enemy.checkVisionRange(this.player.heroBody);
                }, this);
            }
        }

        addEnemySignal(levelOneObjectFactory: LevelOneObjectFactory) {
            levelOneObjectFactory.enemies.forEach((enemy) => {
                enemy.dealDamageSignal.add((damage) => {
                    this.player.takeDamage(damage);
                }, this);
            }, this);
            levelOneObjectFactory.intersectStatueSignal.addOnce((bool) => {
                levelOneObjectFactory.tweenStatue(bool);
            }, this);

        }
    }
}