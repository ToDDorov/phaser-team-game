module GameName {
    export class Minotaur extends WeakEnemy {

        constructor(game, x, y, enemyKey, experience, startingHealth: number, currentHealth: number, startingDamage: number,
                    startingArmor: number, startingMoveSpeed: number, visionRange: number, attackRange: number,
                    idleAnimationFrames: number[],gestureAnimationFrames: number[], moveAnimationFrames: number[],
                    attackAnimationFrames: number[], deathAnimationFrames: number[]) {
            super(game, x, y, enemyKey, experience, startingHealth, currentHealth, startingDamage, startingArmor, startingMoveSpeed,
                visionRange, attackRange, idleAnimationFrames, gestureAnimationFrames, moveAnimationFrames,
                attackAnimationFrames, deathAnimationFrames);

            // this.initialize(MINOTAUR_ENEMY_KEY, MINOTAUR_ENEMY_XP, MINOTAUR_ENEMY_STARTING_HEALTH, MINOTAUR_ENEMY_STARTING_HEALTH,
            //     MINOTAUR_ENEMY_STARTING_DAMAGE, MINOTAUR_ENEMY_STARTING_ARMOR, MINOTAUR_ENEMY_STARTING_MOVE_SPEED,
            //     MINOTAUR_ENEMY_VISION_RANGE, MINOTAUR_ENEMY_ATTACK_RANGE, MINOTAUR_IDLE_ANIMATION_FRAMES,
            //     MINOTAUR_MOVE_ANIMATION_FRAMES, MINOTAUR_ATTACK_ANIMATION_FRAMES, MINOTAUR_DEATH_ANIMATION_FRAMES,
            // MINOTAUR_GESTURE_ANIMATION_FRAMES);

            this.enemyBody.body.setSize(42, 39, 3, 11);
        }
    }
}