TODO:

Everything can be changed, removed or added if needed.

Todor:
Enemy behavior = 90%
Enemy dropping items = 0%
1.fix enemy attack -> DONE
2.add more enemies -> DONE
3.improve level design ->
4.add range enemies ->
5.add boss/bosses ->
6.move information from SelectHero to Game ->

Tisho:
1.Decide how the hero will attack ->
2.Add the rest animations -> DONE
3.Add UI for the health, mana, and maybe stamina for sprint or dash if implemented(depends from the UI sprites) ->
4.Add str, agi, int, exp, armor and inventory attributes to the Hero class and connections between them, etc
str increases overall dmg, agi attack speed, int mana pool->
5.Add more melee heroes ->
6.Add range heroes ->


Megi:
1.Add background music for the menu(menu.ogg or Dream2 or something else)/town(rpg_village2) -> DONE
2.Add sound effects between button switching -> DONE
3.After there are more then 2 heroes, add after "Play game" selection screen with side switching between heroes
with their stats and playing all the animations while waiting to choose the hero ->
4.Add Shop and Skill merchants interface ->
5.Maybe think for story, name of the game, some popup text for NPCs, hero and sighs ->