module GameName {
    export class Enemy extends Entity {
        protected currentState: EnemyStates;
        protected attackAnimationTimer: Phaser.Timer;

        protected enemyVisionRange: number;
        protected enemyAttackRange: number;
        protected distance: number;
        protected visionRange: number;
        protected attackRange: number;

        protected directionNormalized: Phaser.Point;

        protected attackAnimationSignal: Phaser.Signal;
        protected moveSignal: Phaser.Signal;
        protected experienceSignal: Phaser.Signal;

        protected attackAvailable: boolean;

        protected attackAnimationFrames: Array<number>;

        public experience:number;

        enemyBody: Phaser.Sprite;

        constructor(game, x, y,  enemyKey: string, experience:number, startingHealth: number, currentHealth: number,
                    startingDamage: number, startingArmor: number, startingMoveSpeed: number, visionRange: number,
                    attackRange: number, idleAnimationFrames: number[], moveAnimationFrames: number[],
                    attackAnimationFrames: number[], deathAnimationFrames: number[], gestureAnimationFrames?: number[]) {
            super(game, x, y, experience, startingHealth, currentHealth, startingDamage, startingArmor, startingMoveSpeed,
                visionRange, attackRange, idleAnimationFrames, gestureAnimationFrames, moveAnimationFrames,
                attackAnimationFrames, deathAnimationFrames);

        }

        update() {
            // this.drawRange(this.enemyBody.position.x, this.enemyBody.position.y, this.visionRange);
            // this.drawAttackRange(this.enemyBody.position.x - 1, this.enemyBody.position.y + 12,
            //     this.attackRange, 0xFF4F4F);
            // this.game.physics.arcade.collide(this.enemyBody);
            this.onDamageTaken();
        }

        checkVisionRange(heroSprite: Phaser.Sprite): void {
        }

        resetIdleAnimation(): void {
            if (this.currentState !== EnemyStates.IDLE) {
                this.currentState = EnemyStates.IDLE;
                this.attackAvailable = false;
                this.enemyBody.animations.play("idle");
                if (this.attackAnimationTimer) {
                    this.attackAnimationTimer.destroy();
                }
            }
        }

        changeToMoveState(): void {
            if (this.currentState !== EnemyStates.MOVE) {
                this.currentState = EnemyStates.MOVE;
                // bug fixer
                this.enemyBody.animations.getAnimation("attack").destroy();
                this.attackAvailable = false;
                this.enemyBody.animations.play("move");
                if (this.attackAnimationTimer) {
                    this.attackAnimationTimer.destroy();
                }
            }
        }

        changeToAttackState(): void {
            if (this.currentState !== EnemyStates.ATTACK) {
                this.currentState = EnemyStates.ATTACK;
                // bug fixer
                this.enemyBody.animations.add("attack", this.attackAnimationFrames);
                this.attackAvailable = true;
                this.enemyBody.animations.play("idle");
                this.attackAnimationSignal.dispatch(this.attackAvailable);
            }
        }

        resetVelocity(): void {
            this.enemyBody.body.velocity.x = 0;
            this.enemyBody.body.velocity.y = 0;
        }

        changeFacingDirection(hero: Phaser.Sprite): void {
            if (hero.body.x + 3 < this.enemyBody.body.x && this.distance < this.enemyVisionRange) {
                if (this.enemyBody.scale.x === 1 && this.defaultFaceDirection === 1 ||
                    this.enemyBody.scale.x === -1 && this.defaultFaceDirection === 1) {
                    this.enemyBody.scale.x = -1;
                } else {
                    this.enemyBody.scale.x = 1;
                }
            }
            else {
                if (hero.body.x - 3 > this.enemyBody.body.x && this.distance < this.enemyVisionRange) {
                    if (this.enemyBody.scale.x === -1 && this.defaultFaceDirection === 1) {
                        this.enemyBody.scale.x = 1;
                    } else {
                        if (this.defaultFaceDirection !== 1) {
                            this.enemyBody.scale.x = -1;
                        }
                    }
                }
            }
        }

        attackHero(): void {
            if (this.isAlive) {
                this.enemyBody.animations.play("attack", 8, false)
                    .onComplete.addOnce(() => {
                    this.dealDamageSignal.dispatch(this.startingDamage);
                    this.enemyBody.animations.play("idle");
                }, this);
            }
        }

        onDamageTaken() {
            let originalTint = this.enemyBody.tint;
            if (this.enemyBody.tint !== originalTint) {
                this.enemyBody.tint = originalTint;
            }
            if (this.isDamageTaken) {
                this.game.add.tween(this.enemyBody).to({tint: 0xff0000}, 1, Phaser.Easing.Default,
                    true, 0, 0, true);
            }
            this.isDamageTaken = false;
        }

        onDeath() {
            if (this.currentHealth <= 0) {
                this.destroy();
                this.enemyBody.animations.play("death")
                    .onComplete.addOnce(() => {
                        this.experienceSignal.dispatch(this.experience);
                    this.enemyBody.destroy();
                })
            }
        }

    }
}