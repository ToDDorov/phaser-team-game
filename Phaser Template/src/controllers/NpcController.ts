module GameName {
    export class NpcController {
        private HELMET_POSITION: Phaser.Point;
        private ARMOR_POSITION: Phaser.Point;
        private WEAPON_POSITION: Phaser.Point;
        private BELT_POSITION: Phaser.Point;
        private SHIELD_POSITION: Phaser.Point;
        private BOOT_POSITION: Phaser.Point;

        private startingTown: StartingTown;

        private selectedHeroArray: Array<string>;

        private player: Hero;

        private isGearShopOpen: boolean;
        private isSkillShopOpen: boolean;

        private soundController: SoundController;

        private interactButton: Phaser.Key;

        private shopGear: Phaser.Sprite;
        private shopSkill: Phaser.Sprite;
        private inventory: Phaser.Sprite;

        private helmetClicker: Phaser.Sprite;
        private armorClicker: Phaser.Sprite;
        private swordClicker: Phaser.Sprite;
        private beltClicker: Phaser.Sprite;
        private bootsClicker: Phaser.Sprite;
        private shieldClicker: Phaser.Sprite;
        private skillClicker: Phaser.Sprite;
        private item: Phaser.Sprite;

        private itemInfoText: Phaser.Text;
        private skillPackText: Phaser.Text;
        private gearShopTitle: Phaser.Text;
        private skillShopTitle: Phaser.Text;
        private inventoryTitle: Phaser.Text;

        game: Phaser.Game;

        private itemsInfo: any;

        private availableItems: Array<Item>;
        private items: Array<any>;

        constructor(game, startingTown: StartingTown, player: Hero, soundController: SoundController,
                    interactButton: Phaser.Key, selectedHeroArray: Array<string>) {
            this.game = game;
            this.startingTown = startingTown;
            this.player = player;
            this.soundController = soundController;
            this.interactButton = interactButton;
            this.selectedHeroArray = selectedHeroArray;
            this.availableItems = [];
            this.items = [];

            this.HELMET_POSITION = new Phaser.Point(this.game.width * 0.78, this.game.height * 0.67);
            this.ARMOR_POSITION = new Phaser.Point(this.game.width * 0.78, this.game.height * 0.77);
            this.WEAPON_POSITION = new Phaser.Point(this.game.width * 0.73, this.game.height * 0.81);
            this.BELT_POSITION = new Phaser.Point(this.game.width * 0.78, this.game.height * 0.86);
            this.SHIELD_POSITION = new Phaser.Point(this.game.width * 0.825, this.game.height * 0.81);
            this.BOOT_POSITION = new Phaser.Point(this.game.width * 0.78, this.game.height * 0.95);
        }

        create() {
            this.CreateNPC();
            this.createItems();
        }

        update() {
            this.game.physics.arcade.collide(this.player.heroBody, this.startingTown.npcGear.sprite);
            this.game.physics.arcade.collide(this.player.heroBody, this.startingTown.npcSkills.sprite);

            this.interactWithNpc();
        }

        interactWithNpc() {
            if (this.startingTown.npcGearBox !== null) {
                if (this.startingTown.npcGearBox.intersects(this.player.heroBody.body, 0)) {
                    this.startingTown.npcGear.text.alpha = 1;
                    this.player.interactBool = true;

                    if (!this.isGearShopOpen) {
                        if (this.soundController.npcGearLeaving.isPlaying) {
                            this.soundController.npcGearLeaving.stop();
                        }

                        if (this.interactButton.isDown) {
                            this.isGearShopOpen = true;
                            this.interactButton.enabled = false;
                            this.soundController.npcGearWelcome.play().onStop.add(() => {
                                this.soundController.npcGearBuyin.play();
                            });

                            this.game.add.tween(this.inventory).to({alpha: 1}, 100,
                                Phaser.Easing.Default, true, 0, 0, false);
                            this.inventoryTitle.alpha = 1;
                            this.game.add.tween(this.shopGear).to({alpha: 1}, 100,
                                Phaser.Easing.Default, true, 0, 0, false);
                            this.gearShopTitle.alpha = 1;
                            for (let i: number = 0; i < this.availableItems.length - NUM_OF_SKILL_ITEMS; i++) {
                                this.game.add.tween(this.availableItems[i]).to({alpha: 1}, 100,
                                    Phaser.Easing.Default, true, 0, 0, false);
                            }
                        }
                    }
                }
                else {
                    if (this.isGearShopOpen) {
                        this.soundController.npcGearLeaving.play();
                        this.isGearShopOpen = false;
                        this.interactButton.enabled = true;
                        this.startingTown.npcGear.text.alpha = 0;
                    }

                    this.player.interactBool = false;
                    this.startingTown.npcGear.text.alpha = 0;

                    if (this.soundController.npcGearWelcome.isPlaying || this.soundController.npcGearBuyin) {
                        this.soundController.npcGearWelcome.stop();
                        this.soundController.npcGearBuyin.stop();
                    }

                    this.game.add.tween(this.shopGear).to({alpha: 0}, 100,
                        Phaser.Easing.Default, true, 0, 0, false);
                    this.gearShopTitle.alpha = 0;
                    this.game.add.tween(this.inventory).to({alpha: 0}, 100,
                        Phaser.Easing.Default, true, 0, 0, false);
                    this.inventoryTitle.alpha = 0;
                    for (let i: number = 0; i < this.availableItems.length; i++) {
                        this.game.add.tween(this.availableItems[i]).to({alpha: 0}, 100,
                            Phaser.Easing.Default, true, 0, 0, false);
                    }
                }
            }
            if (this.startingTown.npcSkillsBox !== null) {
                if (this.startingTown.npcSkillsBox.intersects(this.player.heroBody.body, 0)) {
                    this.startingTown.npcSkills.text.alpha = 1;
                    this.player.interactBool = true;

                    if (this.interactButton.isDown) {
                        this.soundController.npcSkillWelcome.play();
                        this.isSkillShopOpen = true;
                        this.game.add.tween(this.shopSkill).to({alpha: 1}, 100,
                            Phaser.Easing.Default, true, 0, 0, false);
                        this.skillShopTitle.alpha = 1;
                    }
                    if (this.isSkillShopOpen) {
                        for (let i: number = this.availableItems.length - NUM_OF_SKILL_ITEMS; i < this.availableItems.length; i++) {
                            //console.log(i);
                            this.game.add.tween(this.availableItems[i]).to({alpha: 1}, 200,
                                Phaser.Easing.Default, true, 0, 0, false);
                        }
                    }
                } else {
                    if (this.isSkillShopOpen) {
                        this.startingTown.npcSkills.text.alpha = 0;
                        this.player.interactBool = false;
                        this.isSkillShopOpen = false;
                    }
                    this.startingTown.npcSkills.text.alpha = 0;

                    this.game.add.tween(this.shopSkill).to({alpha: 0}, 100,
                        Phaser.Easing.Default, true, 0, 0, false);
                    this.skillShopTitle.alpha = 0;
                    for (let i: number = this.availableItems.length - NUM_OF_SKILL_ITEMS; i < this.availableItems.length; i++) {
                        this.game.add.tween(this.availableItems[i]).to({alpha: 0}, 100,
                            Phaser.Easing.Default, true, 0, 0, false);
                    }
                    if (this.soundController.npcSkillWelcome.isPlaying) {
                        this.soundController.npcSkillWelcome.stop();
                    }
                }
            }
        }

        CreateNPC() {
            this.inventory = this.game.add.sprite(this.game.width * 0.5, this.game.height * 0.5, "inventory");
            this.inventory.alpha = 0;

            this.shopGear = this.game.add.sprite(this.game.width * 0.2, this.game.height * 0.6, "gearShop");
            this.shopGear.alpha = 0;

            this.shopSkill = this.game.add.sprite(this.game.width * 0.4, this.game.height * 0.6, "skillShop");
            this.shopSkill.alpha = 0;

            this.helmetClicker = this.game.add.sprite(this.inventory.x + 172, this.inventory.y + 57, "clicker");
            this.CreateClicker(this.helmetClicker);
            this.armorClicker = this.game.add.sprite(this.inventory.x + 172, this.inventory.y + 92, "clicker");
            this.CreateClicker(this.armorClicker);
            this.swordClicker = this.game.add.sprite(this.inventory.x + 142, this.inventory.y + 106, "clicker");
            this.CreateClicker(this.swordClicker);
            this.beltClicker = this.game.add.sprite(this.inventory.x + 172, this.inventory.y + 123, "clicker");
            this.CreateClicker(this.beltClicker);
            this.bootsClicker = this.game.add.sprite(this.inventory.x + 172, this.inventory.y + 154, "clicker");
            this.CreateClicker(this.bootsClicker);
            this.shieldClicker = this.game.add.sprite(this.inventory.x + 202, this.inventory.y + 107, "clicker");
            this.CreateClicker(this.shieldClicker);
            this.skillClicker = this.game.add.sprite(this.inventory.x + 45, this.inventory.y + 59, "clicker");
            this.CreateClicker(this.skillClicker);

            let style = {font: "15px Revalia", fill: "#d57c2d", align: "center", stroke: '#000000', strokeThickness: 2};

            this.skillShopTitle = this.game.add.text(this.shopSkill.width * 1.4, this.shopSkill.height * 1.08,
                "SKILL SHOP", style);
            this.skillShopTitle.alpha = 0;

            this.gearShopTitle = new Phaser.Text(this.game, 0, 0, "GEAR SHOP", style);
            this.gearShopTitle.position.set((this.shopGear.width - this.gearShopTitle.width) * 0.5,
                this.shopGear.height * 0.04);
            this.shopGear.addChild(this.gearShopTitle);

            this.inventoryTitle = new Phaser.Text(this.game, 0, 0, "INVENTORY", style);
            this.inventoryTitle.position.set((this.inventory.width - this.inventoryTitle.width) * 0.5,
                this.inventory.height * 0.05);
            this.inventory.addChild(this.inventoryTitle);

            this.itemInfoText = new Phaser.Text(this.game, this.inventory.width * 0.05,
                this.inventory.height * 0.77, "", style);
            this.itemInfoText.fontSize = 10;
            this.itemInfoText.alpha = 0;
            this.itemInfoText.lineSpacing = -10;
            this.itemInfoText.align = "left";
            this.inventory.addChild(this.itemInfoText);

            this.skillPackText = new Phaser.Text(this.game, this.shopSkill.width * 0.6,
                this.shopSkill.height * 0.35, "", style);
            this.skillPackText.fontSize = 10;
            this.skillPackText.lineSpacing = -10;
            this.skillPackText.align = "left";
            this.shopSkill.addChild(this.skillPackText);
        }

        public createItems(): void {
            this.fillGearShopWithItems();
            this.fillSkillShopWithItems();
            for (let i: number = 0; i < this.availableItems.length; i++) {
                this.availableItems[i].events.onDragStop.add(() => {
                    this.onShopItemDragStop(i);
                });

                this.showInfo(i);
                if (i > NUM_OF_GEAR_ITEMS - 1) {
                    this.BuySkillPack(i);
                }
            }
        }

        private BuySkillPack(i: number) {
            this.availableItems[i].events.onInputDown.add(() => {
                if (i > NUM_OF_GEAR_ITEMS - 1 && (i % 3 == 0 || this.availableItems[i - 1].isAvailable == true)) {
                    if (this.player.gold >= this.availableItems[i].price) {
                        this.availableItems[i].tint = TINT_COLOR;
                        this.player.updateSkills(this.availableItems[i].strength, this.availableItems[i].agility,
                            this.availableItems[i].intellect, this.availableItems[i].mana, this.availableItems[i].health,
                            this.availableItems[i].armor, this.availableItems[i].heroDamage);
                        this.player.gold -= this.availableItems[i].price;
                        this.availableItems[i].isAvailable = true;
                    }
                }
            });
        }

        private showInfo(i: number) {
            this.availableItems[i].events.onInputOver.add(() => {
                this.itemInfoText.text = "name: " + this.availableItems[i].name + "\n";

                if (this.availableItems[i].agility != 0) {
                    this.itemInfoText.text += "agility: " + this.availableItems[i].agility + "\n";
                }

                if (this.availableItems[i].armor != 0) {
                    this.itemInfoText.text += "armor: " + this.availableItems[i].armor + "\n";
                }

                if (this.availableItems[i].health != 0) {
                    this.itemInfoText.text += "health: " + this.availableItems[i].health + "\n";
                }

                if (this.availableItems[i].heroDamage != 0) {
                    this.itemInfoText.text += "heroDamage: " + this.availableItems[i].heroDamage + "\n";
                }

                if (this.availableItems[i].intellect != 0) {
                    this.itemInfoText.text += "intellect: " + this.availableItems[i].intellect + "\n";
                }

                if (this.availableItems[i].mana != 0) {
                    this.itemInfoText.text += "mana: " + this.availableItems[i].mana + "\n";
                }

                if (this.availableItems[i].speed != 0) {
                    this.itemInfoText.text += "speed: " + this.availableItems[i].speed + "\n";
                }

                if (this.availableItems[i].strength != 0) {
                    this.itemInfoText.text += "strength: " + this.availableItems[i].strength + "\n";
                }

                if (this.availableItems[i].price != 0) {
                    this.itemInfoText.text += "price: " + this.availableItems[i].price + "\n";
                }

                this.itemInfoText.alpha = 1;
                console.log(this.availableItems[i].name);
            });

            this.availableItems[i].events.onInputOut.add(() => {
                this.itemInfoText.alpha = 0;
                console.log(this.availableItems[i].name);
            });

            this.availableItems[i].events.onInputOver.add(() => {
                this.skillPackText.text = "name: \n" + this.availableItems[i].name + "\n\n";

                if (this.availableItems[i].armor != 0) {
                    this.skillPackText.text += "armor: " + this.availableItems[i].armor + "\n";
                }

                if (this.availableItems[i].health != 0) {
                    this.skillPackText.text += "health: " + this.availableItems[i].health + "\n";
                }

                if (this.availableItems[i].heroDamage != 0) {
                    this.skillPackText.text += "heroDamage: " + this.availableItems[i].heroDamage + "\n";
                }

                if (this.availableItems[i].price != 0) {
                    this.skillPackText.text += "price: " + this.availableItems[i].price + "\n";
                }

                this.skillPackText.alpha = 1;
            });

            this.availableItems[i].events.onInputOut.add(() => {
                this.skillPackText.alpha = 0;
                //console.log(this.availableItems[i].name);
            });
        }

        private fillGearShopWithItems(): void {
            let item: Item;
            let frame: number = 0;

            this.readJSON();
            this.getHeroPack();

            for (let i: number = 0; i < 6; i++) {
                for (let j: number = 0; j < 4; j++) {
                    let positionOfTheHero: Phaser.Point;

                    switch (i) {
                        case 0:
                            positionOfTheHero = this.HELMET_POSITION;
                            break;
                        case 1:
                            positionOfTheHero = this.ARMOR_POSITION;
                            break;
                        case 2:
                            positionOfTheHero = this.WEAPON_POSITION;
                            break;
                        case 3:
                            positionOfTheHero = this.BELT_POSITION;
                            break;
                        case 4:
                            positionOfTheHero = this.SHIELD_POSITION;
                            break;
                        case 5:
                            positionOfTheHero = this.BOOT_POSITION;
                            break;
                    }

                    item = new Item(this.game, this.game.width * 0.235 + j * this.shopGear.width * 0.19,
                        this.game.height * 0.75 + i * this.shopGear.height * 0.102, this.items[frame].frame,
                        this.items[frame].agility, this.items[frame].armor, this.items[frame].health, this.items[frame].heroDamage,
                        this.items[frame].intellect, this.items[frame].mana, this.items[frame].speed,
                        new Phaser.Point(this.game.width * 0.535 + j * this.shopGear.width * 0.19,
                            this.game.height * 0.67 + i * this.shopGear.height * 0.102),
                        positionOfTheHero, this.items[frame].name, this.items[frame].price, this.items[frame].strength);

                    this.game.add.existing(item);

                    item.inputEnabled = true;
                    item.input.enableDrag();
                    item.alpha = 0;

                    this.availableItems.push(item);

                    frame++;
                }
            }
        }

        private fillSkillShopWithItems(): void {
            let item: Item;
            let frame: number = NUM_OF_GEAR_ITEMS;

            this.readJSON();
            this.getHeroPack();

            for (let j: number = 0; j < 3; j++) {
                for (let i: number = 0; i < 3; i++) {
                    let positionOfTheHero: Phaser.Point;

                    switch (i) {
                        case 0:
                            positionOfTheHero = this.HELMET_POSITION;
                            break;
                        case 1:
                            positionOfTheHero = this.ARMOR_POSITION;
                            break;
                        case 2:
                            positionOfTheHero = this.WEAPON_POSITION;
                            break;
                    }

                    item = new Item(this.game, this.game.width * 0.445 + j * this.shopGear.width * 0.28,
                        this.game.height * 0.825 + i * this.shopGear.height * 0.16, this.items[frame].frame,
                        this.items[frame].agility, this.items[frame].armor, this.items[frame].health, this.items[frame].heroDamage,
                        this.items[frame].intellect, this.items[frame].mana, this.items[frame].speed,
                        new Phaser.Point(this.game.width * 0.535 + j * this.shopGear.width * 0.19,
                            this.game.height * 0.67 + i * this.shopGear.height * 0.102),
                        positionOfTheHero, this.items[frame].name, this.items[frame].price,
                        this.items[frame].heroDamage);

                    this.game.add.existing(item);

                    item.inputEnabled = true;
                    item.alpha = 0;

                    this.availableItems.push(item);
                    frame++;
                }
            }
        }

        private readJSON(): void {
            this.itemsInfo = this.game.cache.getJSON(ITEMS_INFO_KEY);
        }

        private getHeroPack(): void {
            for (let i: number = 0; i < this.itemsInfo.packs.length; i++) {
                if (this.itemsInfo.packs[i].heroType == this.selectedHeroArray[0]) {
                    this.items = this.itemsInfo.packs[i].items;
                }
            }
        }

        private onShopItemDragStop(i: number): void {
            if (this.availableItems[i].x > this.inventory.x &&
                this.availableItems[i].x < (this.inventory.x + this.inventory.width) &&
                this.availableItems[i].y > this.inventory.y &&
                this.availableItems[i].y < (this.inventory.y + this.inventory.height)) {
                
                if (this.player.gold >= this.availableItems[i].price) {
                    this.availableItems[i].x = this.availableItems[i].positionInTheInventory.x;
                    this.availableItems[i].y = this.availableItems[i].positionInTheInventory.y;
                    this.player.gold -= this.availableItems[i].price;

                    this.availableItems[i].events.destroy();
                    this.availableItems[i].events.onDragStop.add(() => {
                        this.onHeroItemDragStop(i);
                    });
                } else {
                    this.availableItems[i].x = this.availableItems[i].positionInTheShop.x;
                    this.availableItems[i].y = this.availableItems[i].positionInTheShop.y;
                }

                this.showInfo(i);
            } else {
                this.availableItems[i].x = this.availableItems[i].positionInTheShop.x;
                this.availableItems[i].y = this.availableItems[i].positionInTheShop.y;
            }
        }

        private onHeroItemDragStop(k: number): void {
            if (this.availableItems[k].x > (this.inventory.x + this.inventory.width * 0.6) &&
                this.availableItems[k].x < (this.inventory.x + this.inventory.width) &&
                this.availableItems[k].y > this.inventory.y &&
                this.availableItems[k].y < (this.inventory.y + this.inventory.height)) {

                this.availableItems[k].x = this.availableItems[k].positionOfTheHero.x;
                this.availableItems[k].y = this.availableItems[k].positionOfTheHero.y;

                for (let j: number = 0; j < this.player.itemsInUse.length; j++) {
                    if (this.player.itemsInUse[j].positionOfTheHero == this.availableItems[k].positionOfTheHero) {
                        this.player.itemsInUse[j].x = this.player.itemsInUse[j].positionInTheInventory.x;
                        this.player.itemsInUse[j].y = this.player.itemsInUse[j].positionInTheInventory.y;
                        this.player.updateSkills(-this.player.itemsInUse[j].strength, -this.player.itemsInUse[j].agility,
                            -this.player.itemsInUse[j].intellect, -this.player.itemsInUse[j].mana, -this.player.itemsInUse[j].health);
                        this.player.itemsInUse.splice(j, 1);
                        j--;
                    }
                }
                this.player.itemsInUse.push(this.availableItems[k]);
                this.player.updateSkills(this.availableItems[k].strength, this.availableItems[k].agility,
                    this.availableItems[k].intellect, this.availableItems[k].mana, this.availableItems[k].health);
            } else if (this.availableItems[k].x > this.shopGear.x &&
                this.availableItems[k].x < (this.shopGear.x + this.shopGear.width) &&
                this.availableItems[k].y > this.shopGear.y &&
                this.availableItems[k].y < (this.shopGear.y + this.shopGear.height)) {

                this.availableItems[k].x = this.availableItems[k].positionInTheShop.x;
                this.availableItems[k].y = this.availableItems[k].positionInTheShop.y;
                this.player.gold += this.availableItems[k].price;
                this.availableItems[k].events.destroy();

                for (let m: number = 0; m < this.availableItems.length; m++) {
                    if (this.availableItems[m].positionInTheShop == this.availableItems[k].positionInTheShop)
                        this.availableItems[m].events.onDragStop.add(() => {
                            for (let j: number = 0; j < this.player.itemsInUse.length; j++) {
                                if (this.player.itemsInUse[j].positionOfTheHero == this.availableItems[k].positionOfTheHero) {
                                    this.player.itemsInUse[j].x = this.player.itemsInUse[j].positionInTheInventory.x;
                                    this.player.itemsInUse[j].y = this.player.itemsInUse[j].positionInTheInventory.y;
                                    this.player.updateSkills(-this.player.itemsInUse[j].strength, -this.player.itemsInUse[j].agility,
                                        -this.player.itemsInUse[j].intellect, -this.player.itemsInUse[j].mana, -this.player.itemsInUse[j].health);
                                    this.player.itemsInUse.splice(j, 1);
                                    j--;
                                }
                            }

                            this.onShopItemDragStop(m);
                            this.showInfo(m);
                        });
                }
            } else {
                this.availableItems[k].x = this.availableItems[k].positionInTheInventory.x;
                this.availableItems[k].y = this.availableItems[k].positionInTheInventory.y
                for (let j: number = 0; j < this.player.itemsInUse.length; j++) {
                    if (this.player.itemsInUse[j].positionOfTheHero == this.availableItems[k].positionOfTheHero) {
                        this.player.itemsInUse[j].x = this.player.itemsInUse[j].positionInTheInventory.x;
                        this.player.itemsInUse[j].y = this.player.itemsInUse[j].positionInTheInventory.y;
                        this.player.updateSkills(-this.player.itemsInUse[j].strength, -this.player.itemsInUse[j].agility,
                            -this.player.itemsInUse[j].intellect, -this.player.itemsInUse[j].mana, -this.player.itemsInUse[j].health);
                        this.player.itemsInUse.splice(j, 1);
                        j--;
                    }
                }
            }
        }

        private checkOverlap(spriteA, spriteB) {
            let boundsA = spriteA.getBounds();
            let boundsB = spriteB.getBounds();
            return Phaser.Rectangle.intersects(boundsA, boundsB);
        }

        private CreateClicker(sprite_name: Phaser.Sprite) {
            sprite_name.alpha = 0;
            sprite_name.inputEnabled = true;

            sprite_name.events.onInputOver.add(() => {
                sprite_name.tint = TINT_COLOR;
                //sprite_name.alpha = 0.4;
            });
            sprite_name.events.onInputOut.add(() => {
                sprite_name.tint = 0xFFFFFF;
                sprite_name.alpha = 0;
            });

        }

        destroy() {
            this.HELMET_POSITION = null;
            this.ARMOR_POSITION = null;
            this.WEAPON_POSITION = null;
            this.BELT_POSITION = null;
            this.SHIELD_POSITION = null;
            this.BOOT_POSITION = null;

            this.startingTown.destroy();
            this.startingTown = null;

            for (let i: number = 0; i < this.selectedHeroArray.length; i++) {
                this.selectedHeroArray = null;
            }
            this.selectedHeroArray = [];
            this.selectedHeroArray = null;

            this.player.destroy();
            this.player = null;

            this.isGearShopOpen = null;
            this.isSkillShopOpen = null;

            this.soundController = null;

            this.interactButton = null;

            this.shopGear.destroy();
            this.shopGear = null;
            this.shopSkill.destroy();
            this.shopSkill = null;
            this.inventory.destroy();
            this.inventory = null;

            this.helmetClicker.destroy();
            this.helmetClicker = null;
            this.armorClicker.destroy();
            this.armorClicker = null;
            this.swordClicker.destroy();
            this.swordClicker = null;
            this.beltClicker.destroy();
            this.beltClicker = null;
            this.bootsClicker.destroy();
            this.bootsClicker = null;
            this.shieldClicker.destroy();
            this.shieldClicker = null;
            this.skillClicker.destroy();
            this.skillClicker = null;
            this.item.destroy();
            this.item = null;

            this.gearShopTitle.destroy();
            this.gearShopTitle = null;
            this.skillShopTitle.destroy();
            this.skillShopTitle = null;
            this.inventoryTitle.destroy();
            this.inventoryTitle = null;

            this.itemsInfo = null;

            for (let i: number = 0; i < this.availableItems.length; i++) {
                this.availableItems = null;
            }
            this.availableItems = [];
            this.availableItems = null;

            for (let i: number = 0; i < this.items.length; i++) {
                this.items = null;
            }
            this.items = [];
            this.items = null;
        }
    }
}