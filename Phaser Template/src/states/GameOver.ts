///<reference path="../levels/Level.ts"/>
///<reference path="../levels/StartingTown.ts"/>
///<reference path="../utils/Constants.ts"/>

module GameName {

    export class GameOver extends Phaser.State {
        private button: Phaser.Graphics;

        private resetText: Phaser.Text;
        private gameOverText: Phaser.Text;
        private gameOverBackground: Phaser.Sprite;

        create() {
            this.gameOverBackground = this.game.add.sprite(0, 0, "gameOver");
            let style = {font: "25px Revalia", fill: "#d57c2d", align: "center", stroke: '#000000', strokeThickness: 2};

            this.button = this.game.add.graphics(0, 0); // adds to the world stage
            this.button.beginFill(0xffffff, 0.7);
            this.button.lineStyle(2, 0xFFFFFF, 1);
            this.button.drawRoundedRect((this.game.width - 150) * 0.5, (this.game.height - 50) * 0.7, 150,
                50, 10)
            this.button.inputEnabled = true;

            this.button.events.onInputDown.add(() => {
                this.game.state.start("SelectHero");
            });

            this.resetText = this.game.add.text(this.game.width * 0.5, this.game.height * 0.68, "RESET", style);
            this.resetText.anchor.set(0.5)

            this.gameOverText = this.game.add.text(this.game.width * 0.5, this.game.height * 0.2, "GAME OVER", style);
            this.gameOverText.anchor.set(0.5);
            this.gameOverText.fontSize = 45;
            this.gameOverText.fill = "#ff0000";
            this.gameOverText.stroke = "#d57c2d";
            this.gameOverText.alpha = 0;
        }

        shutdown() {
            this.button.destroy();
            this.button = null;

            this.resetText.destroy();
            this.resetText = null;
            this.gameOverText.destroy();
            this.gameOverText = null;
        }
    }
}