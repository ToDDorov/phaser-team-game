/// <reference path = "../../lib/phaser.d.ts"/>
///<reference path="Level.ts"/>
///<reference path="../gameObjects/Heroes/WarriorHero.ts"/>

module GameName {
    export class StartingTown extends Level{
        door: Phaser.Rectangle;
        npcGearBox: Phaser.Rectangle;
        npcSkillsBox: Phaser.Rectangle;
        npcGear: npcGear;
        npcSkills: npcSkills;

        constructor(game: Phaser.Game){
            super(game);
            this.game = game;
        }

        create() {
            this.map = this.game.add.tilemap(TOWN_TILEMAP_KEY);
            this.map.addTilesetImage(TOWN_TILESET_KEY, TOWN_TILES_KEY);
            this.floor = this.map.createLayer('floor');
            this.collisionBlocks = this.map.createLayer("collision");
            this.map.setCollisionBetween(0, 1100, true, this.collisionBlocks);
            this.walls = this.map.createLayer("walls");
            this.decor = this.map.createLayer("decor");

            let npcGearObject = this.getObjectFromTile('entities', 'npcGear');
            this.npcGear = new npcGear(this.game, npcGearObject.x, npcGearObject.y);
            let npcGearBoxObject = this.getObjectFromTile('entities', 'npcGearBox');
            this.npcGearBox = new Phaser.Rectangle(npcGearBoxObject.x, npcGearBoxObject.y,
                npcGearBoxObject.properties.width, npcGearBoxObject.properties.height);

            let npcSkillsObject = this.getObjectFromTile('entities', 'npcSkills');
            this.npcSkills = new npcSkills(this.game, npcSkillsObject.x , npcSkillsObject.y);
            let npcSkillsBoxObject = this.getObjectFromTile('entities', 'npcSkillsBox');
            this.npcSkillsBox = new Phaser.Rectangle(npcSkillsBoxObject.x, npcSkillsBoxObject.y,
                npcSkillsBoxObject.properties.width, npcSkillsBoxObject.properties.height);

            let doorPosition = this.getObjectFromTile("door", "door");
            this.door = new Phaser.Rectangle(doorPosition.x, doorPosition.y, doorPosition.properties.width,
                doorPosition.properties.height);
            this.group = this.game.add.group();
        }

        destroy() {
            this.npcGear.destroy();
            this.npcSkills.destroy();
            this.door = null;
            this.npcGearBox = null;
            this.npcSkillsBox = null;
        }
    }
}