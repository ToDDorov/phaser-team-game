module GameName {
    export class MageOrc extends WeakEnemy {

        constructor(game, x, y, enemyKey, experience, startingHealth: number, currentHealth: number, startingDamage: number,
                    startingArmor: number, startingMoveSpeed: number, visionRange: number, attackRange: number,
                    idleAnimationFrames: number[],gestureAnimationFrames: number[], moveAnimationFrames: number[],
                    attackAnimationFrames: number[], deathAnimationFrames: number[]) {
            super(game, x, y, enemyKey, experience, startingHealth, currentHealth, startingDamage, startingArmor, startingMoveSpeed,
                visionRange, attackRange, idleAnimationFrames, gestureAnimationFrames, moveAnimationFrames,
                attackAnimationFrames, deathAnimationFrames);

            this.initialize(ORCS_ENEMY_KEY, MAGE_ORC_ENEMY_XP, MAGE_ORC_ENEMY_STARTING_HEALTH, MAGE_ORC_ENEMY_STARTING_HEALTH,
                MAGE_ORC_ENEMY_STARTING_DAMAGE, MAGE_ORC_ENEMY_STARTING_ARMOR, MAGE_ORC_ENEMY_STARTING_MOVE_SPEED,
                MAGE_ORC_ENEMY_VISION_RANGE, MAGE_ORC_ENEMY_ATTACK_RANGE, MAGE_ORC_IDLE_ANIMATION_FRAMES,
                MAGE_ORC_MOVE_ANIMATION_FRAMES, MAGE_ORC_ATTACK_ANIMATION_FRAMES, MAGE_ORC_DEATH_ANIMATION_FRAMES,
                MAGE_ORC_GESTURE_ANIMATION_FRAMES);

            this.enemyBody.body.setSize(25, 32, 4);
            this.enemyBody.scale.set(-1, 1);
            this.defaultFaceDirection = 1;
        }
    }
}