module GameName {
    export class ArmoredGoblin extends WeakEnemy {

        constructor(game, x, y, enemyKey, experience, startingHealth: number, currentHealth: number, startingDamage: number,
                    startingArmor: number, startingMoveSpeed: number, visionRange: number, attackRange: number,
                    idleAnimationFrames: number[],gestureAnimationFrames: number[], moveAnimationFrames: number[],
                    attackAnimationFrames: number[], deathAnimationFrames: number[]) {
            super(game, x, y, enemyKey, experience, startingHealth, currentHealth, startingDamage, startingArmor, startingMoveSpeed,
                visionRange, attackRange, idleAnimationFrames, gestureAnimationFrames, moveAnimationFrames,
                attackAnimationFrames, deathAnimationFrames);

            this.initialize(GOBLINS_ENEMY_KEY, ARMORED_GOBLIN_ENEMY_XP, ARMORED_GOBLIN_ENEMY_STARTING_HEALTH,
                ARMORED_GOBLIN_ENEMY_STARTING_HEALTH, ARMORED_GOBLIN_ENEMY_STARTING_DAMAGE,
                ARMORED_GOBLIN_ENEMY_STARTING_ARMOR, ARMORED_GOBLIN_ENEMY_STARTING_MOVE_SPEED,
                ARMORED_GOBLIN_ENEMY_VISION_RANGE, ARMORED_GOBLIN_ENEMY_ATTACK_RANGE,
                ARMORED_GOBLIN_IDLE_ANIMATION_FRAMES, ARMORED_GOBLIN_MOVE_ANIMATION_FRAMES,
                ARMORED_GOBLIN_ATTACK_ANIMATION_FRAMES, ARMORED_GOBLIN_DEATH_ANIMATION_FRAMES,
                ARMORED_GOBLIN_GESTURE_ANIMATION_FRAMES);

            this.enemyBody.body.setSize(18, 25, 6, 9);
            this.enemyBody.scale.set(-1, 1);
            this.defaultFaceDirection = 1;
        }
    }
}