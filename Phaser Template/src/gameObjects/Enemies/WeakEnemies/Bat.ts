module GameName {
    export class Bat extends WeakEnemy {

        constructor(game, x, y, enemyKey, experience, startingHealth: number, currentHealth: number, startingDamage: number,
                    startingArmor: number, startingMoveSpeed: number, visionRange: number, attackRange: number,
                    idleAnimationFrames: number[],gestureAnimationFrames: number[], moveAnimationFrames: number[],
                    attackAnimationFrames: number[], deathAnimationFrames: number[]) {
            super(game, x, y, enemyKey, experience, startingHealth, currentHealth, startingDamage, startingArmor, startingMoveSpeed,
                visionRange, attackRange, idleAnimationFrames, gestureAnimationFrames, moveAnimationFrames,
                attackAnimationFrames, deathAnimationFrames);

            this.initialize(RAT_AND_BAT_ENEMY_KEY, BAT_ENEMY_XP,BAT_ENEMY_STARTING_HEALTH, BAT_ENEMY_STARTING_HEALTH,
                BAT_ENEMY_STARTING_DAMAGE, BAT_ENEMY_STARTING_ARMOR, BAT_ENEMY_STARTING_MOVE_SPEED,
                BAT_ENEMY_VISION_RANGE, BAT_ENEMY_ATTACK_RANGE, BAT_IDLE_ANIMATION_FRAMES, BAT_MOVE_ANIMATION_FRAMES,
                BAT_ATTACK_ANIMATION_FRAMES, BAT_DEATH_ANIMATION_FRAMES, BAT_GESTURE_ANIMATION_FRAMES);

            this.enemyBody.body.setSize(20, 17, 6, 10);
        }
    }
}