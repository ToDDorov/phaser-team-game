module GameName {
    export class
    Item extends Phaser.Sprite {
        public name: string;

        public agility: number;
        public armor: number;
        public health: number;
        public heroDamage: number;
        public intellect: number;
        public mana: number;
        public speed: number;
        public strength: number;
        public price: number;
        public isAvailable: boolean;

        public positionInTheShop: Phaser.Point;
        public positionInTheInventory: Phaser.Point;
        public positionOfTheHero: Phaser.Point;

        constructor(game: Phaser.Game, x: number, y: number, frame: number, agility: number, armor: number,
                    health: number, heroDamage: number, intellect: number, mana: number, speed: number,
                    positionInTheInventory: Phaser.Point, positionOfTheHero: Phaser.Point, name: string, price: number,
                    strength: number, isAvailable: boolean = null) {
            super(game, x, y, ITEMS_SHEET_KEY, frame);

            this.name = name;

            this.agility = agility;
            this.armor = armor;
            this.health = health;
            this.heroDamage = heroDamage;
            this.intellect = intellect;
            this.mana = mana;
            this.speed = speed;
            this.price = price;
            this.strength = strength;
            this.isAvailable = isAvailable;

            this.positionInTheShop = new Phaser.Point(x, y);
            this.positionInTheInventory = positionInTheInventory;
            this.positionOfTheHero = positionOfTheHero;
        }

        destroy() {
            this.name = null;

            this.agility = null;
            this.armor = null;
            this.health = null;
            this.heroDamage = null;
            this.intellect = null;
            this.mana = null;
            this.speed = null;
            this.strength = null;
            this.price = null;

            this.positionInTheShop = null;
            this.positionInTheInventory = null;
            this.positionOfTheHero = null;
        }

    }
}