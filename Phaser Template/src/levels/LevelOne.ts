module GameName {
    export class LevelOne extends Level {
        public levelOneObjectFactory: LevelOneObjectFactory;

        private miniMapOverlay: Phaser.BitmapData;

        private minimap: Phaser.Sprite;
        private playerBmdOverlay: Phaser.Sprite;

        private playerColor: string;


        constructor(game: Phaser.Game) {
            super(game);
            this.game = game;
        }

        create() {
            this.map = this.game.add.tilemap(LEVEL_ONE_TILEMAP_KEY);
            this.map.addTilesetImage(LEVEL_ONE_TILESET_KEY, LEVEL_ONE_TILES_KEY);
            this.floor = this.map.createLayer("floor");
            this.collisionBlocks = this.map.createLayer("collision");
            this.map.setCollisionBetween(0, 10000, true, this.collisionBlocks);
            this.walls = this.map.createLayer("walls");
            this.decor = this.map.createLayer("decor");
            this.roof = this.map.createLayer("roof");

            this.group = this.game.add.group();

            this.levelOneObjectFactory = new LevelOneObjectFactory(this.game, this.map, this);
            this.levelOneObjectFactory.create();

            let miniMapBmd = this.game.add.bitmapData(this.map.width, this.map.height);

            for (let l = 0; l < this.map.layers.length; l++) {
                for (let y = 0; y < this.map.height; y++) {
                    for (let x = 0; x < this.map.width; x++) {
                        let tile = this.map.getTile(x, y, l);
                        if (tile && this.map.layers[l].name == "collision") {
                            miniMapBmd.ctx.fillStyle = '#fff';
                            miniMapBmd.ctx.fillRect(x , y, 1, 1);
                        }
                    }
                }
            }

            this.minimap = this.game.add.sprite(0, 0, miniMapBmd);
            this.minimap.scale.set(0.5);

            this.miniMapOverlay = this.game.add.bitmapData(this.map.width, this.map.height,);
            this.playerBmdOverlay = this.game.add.sprite(0, 0, this.miniMapOverlay);

            this.playerColor = "#FF0016";

        }

        update(playerPosition: Phaser.Point) {
            let minimapScale = 0.5;
            this.minimap.x = this.game.camera.x + this.game.width * 0.75;
            this.minimap.y = this.game.camera.y + this.game.height * 0.65;

            this.playerBmdOverlay.x = this.minimap.position.x;
            this.playerBmdOverlay.y = this.minimap.position.y;

            this.miniMapOverlay.context.clearRect(0, 0, this.miniMapOverlay.width, this.miniMapOverlay.height);

            this.miniMapOverlay.rect(Math.floor((playerPosition.x * minimapScale) / TILE_SIZE),
                Math.floor((playerPosition.y * minimapScale) / TILE_SIZE), 2, 2, this.playerColor);
            this.miniMapOverlay.dirty = true;
        }
    }
}
