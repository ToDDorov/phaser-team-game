module GameName {
    export class npcSkills extends NPC {

        constructor(game, x, y) {
            super(game, x, y);

            this.sprite = this.game.add.sprite(x, y, NPC_ATLAS_KEY);
            this.sprite.animations.add('idle', ['bearded-idle-1', 'bearded-idle-2', 'bearded-idle-3',
                'bearded-idle-4'], 7, true);
            this.sprite.animations.play("idle");
            this.sprite.scale.setTo(0.8);
            this.sprite.scale.x = -1;
            this.sprite.anchor.set(0.5, 0.5);
            let style = {font: "10px Arial", fill: "#e9e9ff", align: "center", stroke: '#000', strokeThickness: 3};
            this.text = this.game.add.text(x - 45, y - 35, NPC_GEAR_TEXT_SPEECH, style);
            this.text.alpha = 0;
            this.game.physics.arcade.enable(this.sprite);
            this.sprite.body.immovable = true;

        }
    }
}